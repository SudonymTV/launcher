# AmestrisRP - Launcher Documentation
___
# Update Workflow

 - Generate HashList from local Files with Hash Tool (Output is in the binary directory - *Output.xml*)
 - Upload *Output.xml* as **Checksums.xml** onto the Patch Webserver - *SETTINGS Directroy*
 - If the Server IP, Port, or PatchNotes page has been changed, change those values accordingly in the *Launcher.xml* on the Patch Webserver (located in *SETTINGS* Directory) aswell <br /> **Do NOT touch those Elements: ``CR-DL`` - ``L-DL`` - ``remVer``**
# Launcher.xml - Value Description
___
```xml
<option key="CR-IP" value="Folder-Name" />
```
This indicates, what the Folder Name will be in the Users *client_resources* - Please set according to the Server IP.
___
```xml
<option key="CN-IP" value="IP" />
```
This will be the IP-Adress, which will get fed into the Rage:MP Launcher, for the Users to be able to directly connect.
___
```xml
<option key="CN-Port" value="PORT" />
```
This will be the Port, which will get fed into the Rage:MP Launcher aswell, change if default Port has been changed. -  *Default: 22005*
___
```xml
<option key="patchNotes" value="URL" />
```
This is the webPage which is displayed in the Launcher-Tab "Patch-Notes" - If the Url is invalid, I have implemented a Failsafe in the Source Code.

# Notes
- Any Changes you've made on the Patch Webdirectory (SETTINGS) **will be live immediately**
- Users have to restart Launcher (via updater.exe) to grab the newest Settings
- Please remember to Affix dlcpacks in *client_packages\dlcpacks*, if they got updated. - Otherwise Users have to download them via Rage:MP again, and cause the Game to freeze (modload) and them having to restart their Game
