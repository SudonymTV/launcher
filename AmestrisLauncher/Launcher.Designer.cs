﻿namespace AmestrisLauncher
{
    partial class backgroundImage
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonExit = new System.Windows.Forms.Button();
            this.buttonMinimize = new System.Windows.Forms.Button();
            this.buttonInfo = new System.Windows.Forms.Button();
            this.buttonUpdate = new System.Windows.Forms.Button();
            this.buttonPatch = new System.Windows.Forms.Button();
            this.buttonStartseite = new System.Windows.Forms.Button();
            this.smallLogo = new System.Windows.Forms.PictureBox();
            this.buttonPlay = new System.Windows.Forms.Button();
            this.Logo = new System.Windows.Forms.PictureBox();
            this.leftPanel = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.panelHeader = new System.Windows.Forms.Panel();
            this.buttonExtras = new System.Windows.Forms.Button();
            this.extrasSecond1 = new AmestrisLauncher.ExtrasSecond();
            this.extras1 = new AmestrisLauncher.Extras();
            this.info1 = new AmestrisLauncher.Info();
            this.patchNotes1 = new AmestrisLauncher.PatchNotes();
            this.startseite1 = new AmestrisLauncher.Startseite();
            this.update1 = new AmestrisLauncher.Update();
            this.extrasThird1 = new AmestrisLauncher.ExtrasThird();
            ((System.ComponentModel.ISupportInitialize)(this.smallLogo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Logo)).BeginInit();
            this.leftPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonExit
            // 
            this.buttonExit.BackColor = System.Drawing.Color.Transparent;
            this.buttonExit.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Red;
            this.buttonExit.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Red;
            this.buttonExit.Font = new System.Drawing.Font("Bahnschrift", 14F);
            this.buttonExit.Location = new System.Drawing.Point(1055, 0);
            this.buttonExit.Name = "buttonExit";
            this.buttonExit.Size = new System.Drawing.Size(30, 30);
            this.buttonExit.TabIndex = 0;
            this.buttonExit.TabStop = false;
            this.buttonExit.Text = "X";
            this.buttonExit.UseVisualStyleBackColor = false;
            this.buttonExit.Click += new System.EventHandler(this.Button1_Click);
            // 
            // buttonMinimize
            // 
            this.buttonMinimize.BackColor = System.Drawing.Color.Transparent;
            this.buttonMinimize.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Gray;
            this.buttonMinimize.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Gray;
            this.buttonMinimize.Font = new System.Drawing.Font("Bahnschrift", 14F);
            this.buttonMinimize.Location = new System.Drawing.Point(1025, 0);
            this.buttonMinimize.Name = "buttonMinimize";
            this.buttonMinimize.Size = new System.Drawing.Size(30, 30);
            this.buttonMinimize.TabIndex = 3;
            this.buttonMinimize.TabStop = false;
            this.buttonMinimize.Text = "_";
            this.buttonMinimize.UseVisualStyleBackColor = false;
            this.buttonMinimize.Click += new System.EventHandler(this.ButtonMinimize_Click);
            // 
            // buttonInfo
            // 
            this.buttonInfo.BackColor = System.Drawing.Color.Transparent;
            this.buttonInfo.FlatAppearance.BorderSize = 0;
            this.buttonInfo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonInfo.Font = new System.Drawing.Font("Bahnschrift SemiBold", 18F, System.Drawing.FontStyle.Bold);
            this.buttonInfo.ForeColor = System.Drawing.Color.LightGray;
            this.buttonInfo.Location = new System.Drawing.Point(860, 0);
            this.buttonInfo.Name = "buttonInfo";
            this.buttonInfo.Size = new System.Drawing.Size(140, 96);
            this.buttonInfo.TabIndex = 5;
            this.buttonInfo.TabStop = false;
            this.buttonInfo.Text = "Infos";
            this.buttonInfo.UseVisualStyleBackColor = false;
            this.buttonInfo.Click += new System.EventHandler(this.ButtonInfo_Click);
            // 
            // buttonUpdate
            // 
            this.buttonUpdate.BackColor = System.Drawing.Color.Transparent;
            this.buttonUpdate.FlatAppearance.BorderSize = 0;
            this.buttonUpdate.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonUpdate.Font = new System.Drawing.Font("Bahnschrift SemiBold", 18F, System.Drawing.FontStyle.Bold);
            this.buttonUpdate.ForeColor = System.Drawing.Color.LightGray;
            this.buttonUpdate.Location = new System.Drawing.Point(544, 0);
            this.buttonUpdate.Name = "buttonUpdate";
            this.buttonUpdate.Size = new System.Drawing.Size(176, 96);
            this.buttonUpdate.TabIndex = 4;
            this.buttonUpdate.TabStop = false;
            this.buttonUpdate.Text = "Update";
            this.buttonUpdate.UseVisualStyleBackColor = false;
            this.buttonUpdate.Click += new System.EventHandler(this.ButtonUpdate_Click);
            // 
            // buttonPatch
            // 
            this.buttonPatch.BackColor = System.Drawing.Color.Transparent;
            this.buttonPatch.FlatAppearance.BorderSize = 0;
            this.buttonPatch.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonPatch.Font = new System.Drawing.Font("Bahnschrift SemiBold", 18F, System.Drawing.FontStyle.Bold);
            this.buttonPatch.ForeColor = System.Drawing.Color.LightGray;
            this.buttonPatch.Location = new System.Drawing.Point(349, 0);
            this.buttonPatch.Name = "buttonPatch";
            this.buttonPatch.Size = new System.Drawing.Size(195, 96);
            this.buttonPatch.TabIndex = 3;
            this.buttonPatch.TabStop = false;
            this.buttonPatch.Text = "Patch-Notes";
            this.buttonPatch.UseVisualStyleBackColor = false;
            this.buttonPatch.Click += new System.EventHandler(this.ButtonPatch_Click);
            // 
            // buttonStartseite
            // 
            this.buttonStartseite.BackColor = System.Drawing.Color.Transparent;
            this.buttonStartseite.FlatAppearance.BorderSize = 0;
            this.buttonStartseite.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.buttonStartseite.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.buttonStartseite.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonStartseite.Font = new System.Drawing.Font("Bahnschrift SemiBold", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonStartseite.ForeColor = System.Drawing.Color.LightGray;
            this.buttonStartseite.Location = new System.Drawing.Point(164, 0);
            this.buttonStartseite.Name = "buttonStartseite";
            this.buttonStartseite.Size = new System.Drawing.Size(185, 96);
            this.buttonStartseite.TabIndex = 2;
            this.buttonStartseite.TabStop = false;
            this.buttonStartseite.Text = "Startseite";
            this.buttonStartseite.UseVisualStyleBackColor = false;
            this.buttonStartseite.Click += new System.EventHandler(this.ButtonStartseite_Click);
            // 
            // smallLogo
            // 
            this.smallLogo.Image = global::AmestrisLauncher.Properties.Resources.LogoSmallFade;
            this.smallLogo.Location = new System.Drawing.Point(42, 11);
            this.smallLogo.Name = "smallLogo";
            this.smallLogo.Size = new System.Drawing.Size(80, 73);
            this.smallLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.smallLogo.TabIndex = 8;
            this.smallLogo.TabStop = false;
            this.smallLogo.MouseDown += new System.Windows.Forms.MouseEventHandler(this.makeDragable);
            // 
            // buttonPlay
            // 
            this.buttonPlay.BackColor = System.Drawing.Color.Transparent;
            this.buttonPlay.BackgroundImage = global::AmestrisLauncher.Properties.Resources.buttonFade;
            this.buttonPlay.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.buttonPlay.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.buttonPlay.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonPlay.Font = new System.Drawing.Font("Bahnschrift", 26F, System.Drawing.FontStyle.Bold);
            this.buttonPlay.ForeColor = System.Drawing.Color.Black;
            this.buttonPlay.Location = new System.Drawing.Point(27, 470);
            this.buttonPlay.Name = "buttonPlay";
            this.buttonPlay.Size = new System.Drawing.Size(302, 73);
            this.buttonPlay.TabIndex = 6;
            this.buttonPlay.TabStop = false;
            this.buttonPlay.Text = "SPIELEN";
            this.buttonPlay.UseVisualStyleBackColor = false;
            this.buttonPlay.Click += new System.EventHandler(this.ButtonPlay_Click);
            // 
            // Logo
            // 
            this.Logo.BackColor = System.Drawing.Color.Transparent;
            this.Logo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.Logo.ErrorImage = global::AmestrisLauncher.Properties.Resources.logo;
            this.Logo.Image = global::AmestrisLauncher.Properties.Resources.logo;
            this.Logo.ImageLocation = "";
            this.Logo.InitialImage = global::AmestrisLauncher.Properties.Resources.logo;
            this.Logo.Location = new System.Drawing.Point(3, 36);
            this.Logo.Name = "Logo";
            this.Logo.Size = new System.Drawing.Size(352, 135);
            this.Logo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Logo.TabIndex = 0;
            this.Logo.TabStop = false;
            this.Logo.MouseDown += new System.Windows.Forms.MouseEventHandler(this.makeDragable);
            // 
            // leftPanel
            // 
            this.leftPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.leftPanel.Controls.Add(this.label1);
            this.leftPanel.Controls.Add(this.buttonPlay);
            this.leftPanel.Controls.Add(this.Logo);
            this.leftPanel.Location = new System.Drawing.Point(-6, 76);
            this.leftPanel.Name = "leftPanel";
            this.leftPanel.Size = new System.Drawing.Size(355, 578);
            this.leftPanel.TabIndex = 9;
            this.leftPanel.MouseDown += new System.Windows.Forms.MouseEventHandler(this.makeDragable);
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Bahnschrift SemiBold", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(3, 192);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(352, 173);
            this.label1.TabIndex = 7;
            this.label1.Text = "DEUTSCHER GTA5\r\nREALISTIC ROLEPLAY\r\nSERVER AUF BASIS DER\r\nGTA5 MULTIPLAYER\r\nMODIF" +
    "IKATION RAGEMP";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.makeDragable);
            // 
            // panelHeader
            // 
            this.panelHeader.Location = new System.Drawing.Point(-9, 0);
            this.panelHeader.Name = "panelHeader";
            this.panelHeader.Size = new System.Drawing.Size(1106, 96);
            this.panelHeader.TabIndex = 10;
            this.panelHeader.MouseDown += new System.Windows.Forms.MouseEventHandler(this.makeDragable);
            // 
            // buttonExtras
            // 
            this.buttonExtras.BackColor = System.Drawing.Color.Transparent;
            this.buttonExtras.FlatAppearance.BorderSize = 0;
            this.buttonExtras.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.buttonExtras.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.buttonExtras.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonExtras.Font = new System.Drawing.Font("Bahnschrift SemiBold", 18F, System.Drawing.FontStyle.Bold);
            this.buttonExtras.ForeColor = System.Drawing.Color.LightGray;
            this.buttonExtras.Location = new System.Drawing.Point(720, 0);
            this.buttonExtras.Name = "buttonExtras";
            this.buttonExtras.Size = new System.Drawing.Size(140, 96);
            this.buttonExtras.TabIndex = 15;
            this.buttonExtras.TabStop = false;
            this.buttonExtras.Text = "Extras";
            this.buttonExtras.UseVisualStyleBackColor = false;
            this.buttonExtras.Click += new System.EventHandler(this.ButtonExtras_Click);
            // 
            // extrasSecond1
            // 
            this.extrasSecond1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(75)))), ((int)(((byte)(75)))), ((int)(((byte)(75)))), ((int)(((byte)(75)))));
            this.extrasSecond1.Location = new System.Drawing.Point(349, 96);
            this.extrasSecond1.Name = "extrasSecond1";
            this.extrasSecond1.Size = new System.Drawing.Size(736, 555);
            this.extrasSecond1.TabIndex = 17;
            this.extrasSecond1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.makeDragable);
            // 
            // extras1
            // 
            this.extras1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(25)))), ((int)(((byte)(25)))));
            this.extras1.Location = new System.Drawing.Point(349, 96);
            this.extras1.Name = "extras1";
            this.extras1.Size = new System.Drawing.Size(740, 555);
            this.extras1.TabIndex = 16;
            this.extras1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.makeDragable);
            // 
            // info1
            // 
            this.info1.BackColor = System.Drawing.Color.Transparent;
            this.info1.ForeColor = System.Drawing.Color.White;
            this.info1.Location = new System.Drawing.Point(349, 96);
            this.info1.Name = "info1";
            this.info1.Size = new System.Drawing.Size(740, 594);
            this.info1.TabIndex = 13;
            this.info1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.makeDragable);
            // 
            // patchNotes1
            // 
            this.patchNotes1.Location = new System.Drawing.Point(349, 96);
            this.patchNotes1.Name = "patchNotes1";
            this.patchNotes1.Size = new System.Drawing.Size(736, 668);
            this.patchNotes1.TabIndex = 12;
            this.patchNotes1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.makeDragable);
            // 
            // startseite1
            // 
            this.startseite1.BackColor = System.Drawing.Color.Transparent;
            this.startseite1.Location = new System.Drawing.Point(349, 96);
            this.startseite1.Name = "startseite1";
            this.startseite1.Size = new System.Drawing.Size(740, 566);
            this.startseite1.TabIndex = 11;
            this.startseite1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.makeDragable);
            // 
            // update1
            // 
            this.update1.BackColor = System.Drawing.Color.Transparent;
            this.update1.Location = new System.Drawing.Point(349, 96);
            this.update1.Name = "update1";
            this.update1.Size = new System.Drawing.Size(735, 562);
            this.update1.TabIndex = 14;
            this.update1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.makeDragable);
            // 
            // extrasThird1
            // 
            this.extrasThird1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(75)))), ((int)(((byte)(75)))), ((int)(((byte)(75)))), ((int)(((byte)(75)))));
            this.extrasThird1.Location = new System.Drawing.Point(350, 97);
            this.extrasThird1.Name = "extrasThird1";
            this.extrasThird1.Size = new System.Drawing.Size(747, 554);
            this.extrasThird1.TabIndex = 18;
            // 
            // backgroundImage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(25)))), ((int)(((byte)(25)))));
            this.BackgroundImage = global::AmestrisLauncher.Properties.Resources.bgscnd;
            this.ClientSize = new System.Drawing.Size(1086, 651);
            this.Controls.Add(this.patchNotes1);
            this.Controls.Add(this.extrasThird1);
            this.Controls.Add(this.extrasSecond1);
            this.Controls.Add(this.extras1);
            this.Controls.Add(this.buttonInfo);
            this.Controls.Add(this.buttonExtras);
            this.Controls.Add(this.info1);
            this.Controls.Add(this.buttonUpdate);
            this.Controls.Add(this.startseite1);
            this.Controls.Add(this.update1);
            this.Controls.Add(this.smallLogo);
            this.Controls.Add(this.buttonMinimize);
            this.Controls.Add(this.buttonExit);
            this.Controls.Add(this.buttonStartseite);
            this.Controls.Add(this.buttonPatch);
            this.Controls.Add(this.panelHeader);
            this.Controls.Add(this.leftPanel);
            this.ForeColor = System.Drawing.SystemColors.ControlText;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "backgroundImage";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "AmestrisRP - Launcher";
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.makeDragable);
            ((System.ComponentModel.ISupportInitialize)(this.smallLogo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Logo)).EndInit();
            this.leftPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonExit;
        private System.Windows.Forms.PictureBox Logo;
        private System.Windows.Forms.Button buttonStartseite;
        private System.Windows.Forms.Button buttonInfo;
        private System.Windows.Forms.Button buttonUpdate;
        private System.Windows.Forms.Button buttonPatch;
        private System.Windows.Forms.Button buttonPlay;
        private System.Windows.Forms.Button buttonMinimize;
        private System.Windows.Forms.PictureBox smallLogo;
        private System.Windows.Forms.Panel leftPanel;
        private System.Windows.Forms.Panel panelHeader;
        private PatchNotes patchNotes1;
        private Info info1;
        private Update update1;
        public Startseite startseite1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button buttonExtras;
        private Extras extras1;
        private ExtrasSecond extrasSecond1;
        private ExtrasThird extrasThird1;
    }
}

