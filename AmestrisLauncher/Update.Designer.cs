﻿namespace AmestrisLauncher
{
    partial class Update
    {
        /// <summary> 
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Komponenten-Designer generierter Code

        /// <summary> 
        /// Erforderliche Methode für die Designerunterstützung. 
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.UpdateStart = new System.Windows.Forms.Button();
            this.resultLabel = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.pathChoice = new System.Windows.Forms.Button();
            this.resultLabelNew = new System.Windows.Forms.Label();
            this.metroProgressBar1 = new MetroFramework.Controls.MetroProgressBar();
            this.SuspendLayout();
            // 
            // UpdateStart
            // 
            this.UpdateStart.BackColor = System.Drawing.Color.Transparent;
            this.UpdateStart.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.UpdateStart.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.UpdateStart.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.UpdateStart.Font = new System.Drawing.Font("Bahnschrift", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.UpdateStart.ForeColor = System.Drawing.Color.Black;
            this.UpdateStart.Location = new System.Drawing.Point(123, 464);
            this.UpdateStart.Name = "UpdateStart";
            this.UpdateStart.Size = new System.Drawing.Size(447, 55);
            this.UpdateStart.TabIndex = 0;
            this.UpdateStart.TabStop = false;
            this.UpdateStart.Text = "Update starten";
            this.UpdateStart.UseVisualStyleBackColor = false;
            this.UpdateStart.Click += new System.EventHandler(this.UpdateStart_Click);
            // 
            // resultLabel
            // 
            this.resultLabel.BackColor = System.Drawing.Color.Transparent;
            this.resultLabel.Font = new System.Drawing.Font("Bahnschrift SemiBold", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.resultLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(210)))), ((int)(((byte)(210)))));
            this.resultLabel.Location = new System.Drawing.Point(97, 227);
            this.resultLabel.Name = "resultLabel";
            this.resultLabel.Size = new System.Drawing.Size(275, 25);
            this.resultLabel.TabIndex = 2;
            this.resultLabel.Text = "Warte auf RageMP Pfad ....";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Bahnschrift SemiBold", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(-10, 404);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(705, 46);
            this.label1.TabIndex = 3;
            this.label1.Text = "  Bevor Du das  Update starten kannst, musst Du Deinen RageMP Installationspfad\r\n" +
    "                       angeben, damit wir wissen wo die Dateien gespeichert werd" +
    "en.";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Bahnschrift SemiBold", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(9, 101);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(271, 25);
            this.label2.TabIndex = 4;
            this.label2.Text = "RageMP Installationspfad ...";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Bahnschrift", 28F, System.Drawing.FontStyle.Bold);
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(249, 32);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(184, 46);
            this.label3.TabIndex = 5;
            this.label3.Text = "UPDATER";
            // 
            // pathChoice
            // 
            this.pathChoice.BackColor = System.Drawing.Color.Transparent;
            this.pathChoice.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.pathChoice.Font = new System.Drawing.Font("Bahnschrift SemiBold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pathChoice.ForeColor = System.Drawing.Color.White;
            this.pathChoice.Location = new System.Drawing.Point(13, 130);
            this.pathChoice.Name = "pathChoice";
            this.pathChoice.Size = new System.Drawing.Size(93, 30);
            this.pathChoice.TabIndex = 6;
            this.pathChoice.TabStop = false;
            this.pathChoice.Text = "Suchen ...";
            this.pathChoice.UseVisualStyleBackColor = false;
            this.pathChoice.Click += new System.EventHandler(this.PathChoice_Click);
            // 
            // resultLabelNew
            // 
            this.resultLabelNew.BackColor = System.Drawing.Color.Transparent;
            this.resultLabelNew.Font = new System.Drawing.Font("Bahnschrift SemiCondensed", 18F, System.Drawing.FontStyle.Bold);
            this.resultLabelNew.ForeColor = System.Drawing.Color.White;
            this.resultLabelNew.Location = new System.Drawing.Point(-1, 240);
            this.resultLabelNew.Name = "resultLabelNew";
            this.resultLabelNew.Size = new System.Drawing.Size(683, 27);
            this.resultLabelNew.TabIndex = 8;
            this.resultLabelNew.Text = "Wartet ...";
            this.resultLabelNew.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // metroProgressBar1
            // 
            this.metroProgressBar1.Location = new System.Drawing.Point(5, 273);
            this.metroProgressBar1.Name = "metroProgressBar1";
            this.metroProgressBar1.Size = new System.Drawing.Size(674, 29);
            this.metroProgressBar1.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroProgressBar1.TabIndex = 9;
            this.metroProgressBar1.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // Update
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(25)))), ((int)(((byte)(25)))));
            this.Controls.Add(this.metroProgressBar1);
            this.Controls.Add(this.resultLabelNew);
            this.Controls.Add(this.pathChoice);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.UpdateStart);
            this.Name = "Update";
            this.Size = new System.Drawing.Size(816, 661);
            this.Load += new System.EventHandler(this.Update_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button UpdateStart;
        private System.Windows.Forms.Label resultLabel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button pathChoice;
        private System.Windows.Forms.Label resultLabelNew;
        private MetroFramework.Controls.MetroProgressBar metroProgressBar1;
    }
}
