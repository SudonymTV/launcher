﻿namespace AmestrisLauncher
{
    partial class PatchNotes
    {
        /// <summary> 
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Komponenten-Designer generierter Code

        /// <summary> 
        /// Erforderliche Methode für die Designerunterstützung. 
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.patchBrowser = new System.Windows.Forms.WebBrowser();
            this.SuspendLayout();
            // 
            // patchBrowser
            // 
            this.patchBrowser.AllowNavigation = false;
            this.patchBrowser.AllowWebBrowserDrop = false;
            this.patchBrowser.Dock = System.Windows.Forms.DockStyle.Fill;
            this.patchBrowser.IsWebBrowserContextMenuEnabled = false;
            this.patchBrowser.Location = new System.Drawing.Point(0, 0);
            this.patchBrowser.MinimumSize = new System.Drawing.Size(20, 20);
            this.patchBrowser.Name = "patchBrowser";
            this.patchBrowser.ScriptErrorsSuppressed = true;
            this.patchBrowser.Size = new System.Drawing.Size(820, 515);
            this.patchBrowser.TabIndex = 0;
            this.patchBrowser.TabStop = false;
            this.patchBrowser.WebBrowserShortcutsEnabled = false;
            this.patchBrowser.DocumentCompleted += new System.Windows.Forms.WebBrowserDocumentCompletedEventHandler(this.PatchBrowser_DocumentCompleted);
            // 
            // PatchNotes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.patchBrowser);
            this.Name = "PatchNotes";
            this.Size = new System.Drawing.Size(820, 515);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.WebBrowser patchBrowser;
    }
}
