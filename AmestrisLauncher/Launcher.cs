﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Xml.Linq;
using System.IO;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing.Imaging;
using Microsoft.Win32;
using DiscordRPC;
using DiscordRPC.Logging;
using System.Timers;

namespace AmestrisLauncher
{
    public partial class backgroundImage : Form
    {
        readonly Dictionary<String, string> Options = new Dictionary<String, String>();

        //Discord
        public DiscordRpcClient client;

        //Stuff to make borderless window dragable
        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;

        [DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();

        private void makeDragable(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }

        //Rounded Corners
        [DllImport("Gdi32.dll", EntryPoint = "CreateRoundRectRgn")]
        private static extern IntPtr CreateRoundRectRgn
        (
            int nLeftRect,     // x-coordinate of upper-left corner
            int nTopRect,      // y-coordinate of upper-left corner
            int nRightRect,    // x-coordinate of lower-right corner
            int nBottomRect,   // y-coordinate of lower-right corner
            int nWidthEllipse, // height of ellipse
            int nHeightEllipse // width of ellipse
        );

        private void BackgroundImage_Load(object sender, System.EventArgs e)
        {
            //Main Opacity
            leftPanel.BackColor = Color.FromArgb(175, 0, 0, 0);
            //Button Opacity
            panelHeader.BackColor = Color.FromArgb(175, 0, 0, 0);
            buttonInfo.BackColor = Color.FromArgb(175, 0, 0, 0);
            buttonExtras.BackColor = Color.FromArgb(175, 0, 0, 0);
            buttonUpdate.BackColor = Color.FromArgb(175, 0, 0, 0);
            buttonStartseite.BackColor = Color.FromArgb(175, 0, 0, 0);
            buttonPatch.BackColor = Color.FromArgb(175, 0, 0, 0);
            buttonExit.BackColor = Color.FromArgb(175, 0, 0, 0);
            buttonMinimize.BackColor = Color.FromArgb(175, 0, 0, 0);
            smallLogo.BackColor = Color.FromArgb(175, 0, 0, 0);
            //Panels Opacity
            update1.BackColor = Color.FromArgb(175, 0, 0, 0);
            info1.BackColor = Color.FromArgb(175, 0, 0, 0);
            patchNotes1.BackColor = Color.FromArgb(175, 0, 0, 0);
            startseite1.BackColor = Color.FromArgb(175, 0, 0, 0);
            extras1.BackColor = Color.FromArgb(175, 0, 0, 0);
            extrasSecond1.BackColor = Color.FromArgb(175, 0, 0, 0);
            extrasThird1.BackColor = Color.FromArgb(175, 0, 0, 0);

            //Strech Background Image to Panel Size
            BackgroundImageLayout = ImageLayout.Stretch;

            //Set Icon
            this.Icon = Properties.Resources.IconTray;

            //Disable all Buttons until logged in
            buttonStartseite.Enabled = true;
            buttonPatch.Enabled = true;
            buttonUpdate.Enabled = true;
            buttonInfo.Enabled = true;
            buttonExtras.Enabled = true;

            //Button Play Design
            buttonPlay.BackColor = Color.Transparent;
            buttonPlay.BackgroundImageLayout = ImageLayout.Stretch;
            buttonPlay.BackgroundImage = SetImageOpacity(Properties.Resources.buttonFade, 0.8F);

            //Set Name
            this.Name = "AmestrisRP - Launcher";

            //Button Hover Event
            buttonPlay.MouseEnter += new EventHandler(ButtonPlay_MouseEnter);
            buttonPlay.MouseLeave += new EventHandler(ButtonPlay_MouseLeave);
            buttonStartseite.MouseEnter += new EventHandler(ButtonStartseite_MouseEnter);
            buttonStartseite.MouseLeave += new EventHandler(ButtonStartseite_MouseLeave);
            buttonPatch.MouseEnter += new EventHandler(ButtonPatch_MouseEnter);
            buttonPatch.MouseLeave += new EventHandler(ButtonPatch_MouseLeave);
            buttonUpdate.MouseEnter += new EventHandler(ButtonUpdate_MouseEnter);
            buttonUpdate.MouseLeave += new EventHandler(ButtonUpdate_MouseLeave);
            buttonInfo.MouseEnter += new EventHandler(ButtonInfo_MouseEnter);
            buttonInfo.MouseLeave += new EventHandler(ButtonInfo_MouseLeave);
            buttonExtras.MouseEnter += new EventHandler(ButtonExtras_MouseEnter);
            buttonExtras.MouseLeave += new EventHandler(ButtonExtras_MouseLeave);

            //Round Corners
            Region = System.Drawing.Region.FromHrgn(CreateRoundRectRgn(0, 0, Width, Height, 12, 12));

            //Show Startseite and overlay login window on top of that, so it can get displayed if user is logged in
            startseite1.Show();
            update1.Hide();
            patchNotes1.Hide();
            info1.Hide();
            extras1.Hide();
            extrasSecond1.Hide();
            extrasThird1.Hide();

            //Button-Startseite Settings
            buttonStartseite.FlatStyle = FlatStyle.Flat;
            buttonStartseite.FlatAppearance.BorderSize = 0;
            buttonStartseite.FlatAppearance.BorderColor = Color.FromArgb(0, 255, 255, 255); //Set no Border

            //Button-Patchnotes Settings
            buttonPatch.FlatStyle = FlatStyle.Flat;
            buttonPatch.FlatAppearance.BorderSize = 0;
            buttonPatch.FlatAppearance.BorderColor = Color.FromArgb(0, 255, 255, 255); //Set no Border

            //Button-Update Settings
            buttonUpdate.FlatStyle = FlatStyle.Flat;
            buttonUpdate.FlatAppearance.BorderSize = 0;
            buttonUpdate.FlatAppearance.BorderColor = Color.FromArgb(0, 255, 255, 255); //Set no Border

            //Button-Infos Settings
            buttonInfo.FlatStyle = FlatStyle.Flat;
            buttonInfo.FlatAppearance.BorderSize = 0;
            buttonInfo.FlatAppearance.BorderColor = Color.FromArgb(0, 255, 255, 255); //Set no Border

            //Button-Play Settings
            buttonPlay.FlatStyle = FlatStyle.Flat;
            buttonPlay.FlatAppearance.BorderSize = 0;
            buttonPlay.FlatAppearance.BorderColor = Color.FromArgb(0, 255, 255, 255); //Set no Border
            buttonPlay.Enabled = false;

            //Button-Exit Settings
            buttonExit.FlatStyle = FlatStyle.Flat;
            buttonExit.FlatAppearance.BorderSize = 0;
            buttonExit.FlatAppearance.BorderColor = Color.FromArgb(0, 255, 255, 255); //Set no Border

            //Button-Extras Settings
            buttonExtras.FlatStyle = FlatStyle.Flat;
            buttonExtras.FlatAppearance.BorderSize = 0;
            buttonExtras.FlatAppearance.BorderColor = Color.FromArgb(0, 255, 255, 255); //Set no Border

            //Button-Minimize Settings
            buttonMinimize.FlatStyle = FlatStyle.Flat;
            buttonMinimize.FlatAppearance.BorderSize = 0;
            buttonMinimize.FlatAppearance.BorderColor = Color.FromArgb(0, 255, 255, 255); //Set no Border
        }

        public backgroundImage()
        {
            //Initialize
            Load += new EventHandler(BackgroundImage_Load);
            this.DoubleBuffered = true;
            InitializeComponent();

            // Discord
            client = new DiscordRpcClient("586510402398060584");
            client.Logger = new ConsoleLogger() { Level = LogLevel.Warning };
            dynamic DateTimestampEnd = null;
            client.OnReady += (sender, e) =>
            {
                Console.WriteLine("Received Ready from user {0}", e.User.Username);
            };
            client.OnPresenceUpdate += (sender, e) =>
            {
                Console.WriteLine("Received Update! {0}", e.Presence);
            };
            client.Initialize();
            client.SetPresence(new RichPresence()
            {
                Details = "SoonTM - (Soon von 300)",
                State = "Bestaunt den Launcher ...",

                Timestamps = new Timestamps()
                {
                    Start = DateTime.UtcNow,
                    End = DateTimestampEnd
                },
                Assets = new Assets()
                {
                    LargeImageKey = "big",
                    LargeImageText = "AmestrisRP, basierend auf der Modifikation RageMP.",
                    SmallImageKey = "small_final",
                    SmallImageText = "You found my Secret... Don't tell anybody!!! - Sudonym 2019"

                }
            });


            //Check if Update.exe has been launched beforehand
            string tempUpdate = AppDomain.CurrentDomain.BaseDirectory + "\\.updaterOpened";
            if (!File.Exists(tempUpdate))
            {
                MessageBox.Show("Du hast den Launcher gestartet, ohne die Updater.exe vorher zu starten!\n\nBitte starte den Launcher NUR über die Updater.exe!", "Fehler!",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                Application.Exit();
                Process.GetCurrentProcess().Kill();
                Environment.Exit(0);
            } else if (File.Exists(tempUpdate))
            {
                File.Delete(tempUpdate);
            } 

            var settings = AppDomain.CurrentDomain.BaseDirectory + "\\Launcher.xml";
            if (File.Exists(settings))
            {
                try
                {
                    foreach (var el in XElement.Load(File.OpenRead(settings)).Elements())
                    {
                        Options.Add(el.Attribute("key").Value, el.Attribute("value").Value);
                    }
                }
                catch { ReportError("Ein Fehler ist während dem übernehmen der Einstellungen aufgetreten! - (Preload)"); return; }
            }
            else
            {
                ReportError("Kann 'Launcher.xml' nicht finden!");
                return;
            }
        }


        private void ButtonPlay_MouseEnter(object sender, EventArgs e)
        {
            buttonPlay.ForeColor = Color.GhostWhite;
        }
        private void ButtonPlay_MouseLeave(object sender, EventArgs e)
        {
            buttonPlay.ForeColor = Color.Black;
        }

        private void ButtonStartseite_MouseEnter(object sender, EventArgs e)
        {
            buttonStartseite.BackColor = Color.FromArgb(175, 0, 0, 0);
            buttonStartseite.FlatAppearance.MouseDownBackColor = Color.FromArgb(175, 0, 0, 0);
            buttonStartseite.FlatAppearance.MouseOverBackColor = Color.FromArgb(175, 0, 0, 0);
            buttonStartseite.ForeColor = Color.White;
        }
        private void ButtonStartseite_MouseLeave(object sender, EventArgs e)
        {
            buttonStartseite.ForeColor = Color.LightGray;
        }

        private void ButtonPatch_MouseEnter(object sender, EventArgs e)
        {
            buttonPatch.BackColor = Color.FromArgb(175, 0, 0, 0);
            buttonPatch.FlatAppearance.MouseDownBackColor = Color.FromArgb(175, 0, 0, 0);
            buttonPatch.FlatAppearance.MouseOverBackColor = Color.FromArgb(175, 0, 0, 0);
            buttonPatch.ForeColor = Color.White;
        }
        private void ButtonPatch_MouseLeave(object sender, EventArgs e)
        {
            buttonPatch.ForeColor = Color.LightGray;
        }

        private void ButtonUpdate_MouseEnter(object sender, EventArgs e)
        {
            buttonUpdate.BackColor = Color.FromArgb(175, 0, 0, 0);
            buttonUpdate.FlatAppearance.MouseDownBackColor = Color.FromArgb(175, 0, 0, 0);
            buttonUpdate.FlatAppearance.MouseOverBackColor = Color.FromArgb(175, 0, 0, 0);
            buttonUpdate.ForeColor = Color.White;
        }
        private void ButtonUpdate_MouseLeave(object sender, EventArgs e)
        {
            buttonUpdate.ForeColor = Color.LightGray;
        }

        private void ButtonInfo_MouseEnter(object sender, EventArgs e)
        {
            buttonInfo.BackColor = Color.FromArgb(175, 0, 0, 0);
            buttonInfo.FlatAppearance.MouseDownBackColor = Color.FromArgb(175, 0, 0, 0);
            buttonInfo.FlatAppearance.MouseOverBackColor = Color.FromArgb(175, 0, 0, 0);
            buttonInfo.ForeColor = Color.White;
        }
        private void ButtonInfo_MouseLeave(object sender, EventArgs e)
        {
            buttonInfo.ForeColor = Color.LightGray;
        }

        private void ButtonExtras_MouseEnter(object sender, EventArgs e)
        {
            buttonExtras.BackColor = Color.FromArgb(175, 0, 0, 0);
            buttonExtras.FlatAppearance.MouseDownBackColor = Color.FromArgb(175, 0, 0, 0);
            buttonExtras.FlatAppearance.MouseOverBackColor = Color.FromArgb(175, 0, 0, 0);
            buttonExtras.ForeColor = Color.White;
        }
        private void ButtonExtras_MouseLeave(object sender, EventArgs e)
        {
            buttonExtras.ForeColor = Color.LightGray;
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void ButtonMinimize_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void ButtonStartseite_Click(object sender, EventArgs e)
        {
            //Hide Others
            patchNotes1.Hide();
            info1.Hide();
            update1.Hide();
            extras1.Hide();
            //Show Current
            startseite1.Show();
            startseite1.BringToFront();
        }

        private void ButtonPatch_Click(object sender, EventArgs e)
        {
            //Hide Others
            startseite1.Hide();
            info1.Hide();
            update1.Hide();
            extras1.Hide();
            //Show Current
            patchNotes1.Show();
            patchNotes1.BringToFront();
        }

        private void ButtonUpdate_Click(object sender, EventArgs e)
        {
            //Hide Others
            startseite1.Hide();
            info1.Hide();
            patchNotes1.Hide();
            extras1.Hide();
            //Show Current
            update1.Show();
            update1.BringToFront();
        }

        private void ButtonInfo_Click(object sender, EventArgs e)
        {
            //Hide Others
            startseite1.Hide();
            update1.Hide();
            patchNotes1.Hide();
            extras1.Hide();
            //Show Current
            info1.Show();
            info1.BringToFront();
        }

        private void ButtonExtras_Click(object sender, EventArgs e)
        {
            //Hide Others
            startseite1.Hide();
            update1.Hide();
            patchNotes1.Hide();
            info1.Hide();
            //Show Current
            extras1.Show();
            extras1.BringToFront();
        }

        private static void ReportError(String msg)
        {
            MessageBox.Show(msg, "Fehler!",
                MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        public Image SetImageOpacity(Image image, float opacity)
        {
            Bitmap bmp = new Bitmap(image.Width, image.Height);
            using (Graphics g = Graphics.FromImage(bmp))
            {
                ColorMatrix matrix = new ColorMatrix
                {
                    Matrix33 = opacity
                };
                ImageAttributes attributes = new ImageAttributes();
                attributes.SetColorMatrix(matrix, ColorMatrixFlag.Default,
                                                  ColorAdjustType.Bitmap);
                g.DrawImage(image, new Rectangle(0, 0, bmp.Width, bmp.Height),
                                   0, 0, image.Width, image.Height,
                                   GraphicsUnit.Pixel, attributes);
            }
            return bmp;
        }

        private void ButtonPlay_Click(object sender, EventArgs e)
        {
            //Update Registry with Server IP
            Registry.SetValue(@"HKEY_CURRENT_USER\Software\RAGE-MP", "launch.ip", Options["CN-IP"]);
            Registry.SetValue(@"HKEY_CURRENT_USER\Software\RAGE-MP", "launch.port", Options["CN-Port"]);

            //Set Discord-RPC and set the Data again after three Minutes to overwrite RageMPs RPC
            client.UpdateState("Spielt auf dem Server ...");
            System.Timers.Timer rpcTimer = new System.Timers.Timer();
            rpcTimer.Elapsed += new ElapsedEventHandler(setRPC);
            rpcTimer.Interval = 180000;
            rpcTimer.AutoReset = false;
            rpcTimer.Enabled = true;

            RegistryKey key = Registry.CurrentUser.OpenSubKey("Amestris", true);
            if (key.GetValue("RageMP-Path") != null)
            {
                buttonPlay.Enabled = false;
                string path = key.GetValue("RageMP-Path").ToString();
                ProcessStartInfo ps1 = new ProcessStartInfo(path + "\\updater.exe")
                {
                    WorkingDirectory = path + "\\"
                };
                Process.Start(ps1);
            }
        }

        private void setRPC(object source, ElapsedEventArgs e)
        {
            dynamic DateTimestampEnd = null;

            client.SetPresence(new RichPresence()
            {
                Details = "SoonTM - (Soon von 300)",
                State = "Spielt auf dem Server ...",

                Timestamps = new Timestamps()
                {
                    Start = DateTime.UtcNow,
                    End = DateTimestampEnd
                },
                Assets = new Assets()
                {
                    LargeImageKey = "big",
                    LargeImageText = "AmestrisRP, basierend auf der Modifikation RageMP.",
                    SmallImageKey = "small_final",
                    SmallImageText = "You found my Secret... Don't tell anybody!!! - Sudonym 2019"

                }
            });

            Console.WriteLine("[FIX-RageRPC] - Set AmestrisRP Discord RPC Data.");
        }
    }
}
