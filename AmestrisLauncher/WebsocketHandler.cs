﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Net;
using System.Net.Http;

namespace AmestrisLauncher
{
    public partial class WebsocketHandler : Component
    {
        public WebsocketHandler()
        {
            InitializeComponent();
        }

        //Announce User to Websocket, which causes a Field to get updated with the current Announce Timestamp (will get used by Server, to decide, wether or not Launcher has been opened in the last 5 Minutes)
        public static void AnnounceUser()
        {
            using (var client = new HttpClient())
            {
                var content = new FormUrlEncodedContent(new Dictionary<string, string>
                    {
                        { "ip_address", Program.IpAddress },
                    });

                //var response = client.PostAsync("https://amestris.haliax.net/launcher/announce", content).GetAwaiter().GetResult();
            }
        }

        public WebsocketHandler(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }
    }
}
