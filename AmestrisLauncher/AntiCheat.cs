﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Http;

namespace AmestrisLauncher
{
    class AntiCheat
    {
        private static object _obj = new object();

        public static void ScanProcesses()
        {
            lock (_obj)
            {
                Process[] pname = Process.GetProcessesByName("gta5");
                if (pname.Length > 0)
                {
                    foreach (Process p in Process.GetProcesses())
                    {
                        if (p.ProcessName.Contains("cheatengine"))
                        {
                            Announce(p.ProcessName);
                            continue;
                        }

                        if (p.ProcessName.Contains("modmenu") || p.ProcessName.Contains("moddedmenu"))
                        {
                            Announce(p.ProcessName);
                            continue;
                        }

                        if (p.ProcessName.Contains("AutoHotkey") || p.ProcessName.Contains("AutoHotkey Unicode"))
                        {
                            Announce(p.ProcessName);
                            continue;
                        }

                        if (p.ProcessName.Contains("AutoClicker") || p.ProcessName.Contains("Speed AutoClicker"))
                        {
                            Announce(p.ProcessName);
                            continue;
                        }
                    }
                }
            }
        }

        private static void Announce(string name)
        {
            using (var client = new System.Net.Http.HttpClient())
            {
                var content = new FormUrlEncodedContent(new Dictionary<string, string>
                    {
                        { "ip_address", Program.IpAddress },
                        { "target",  Base64Encode(name) },
                    });

                var response = client.PostAsync("https://amestris.haliax.net/launcher/hit", content).GetAwaiter().GetResult();
            }
        }

        public static string Base64Encode(string plainText)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes);
        }
    }
}
