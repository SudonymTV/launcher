﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Net;
using System.Threading;
using System.Windows.Forms;
using System.Windows.Threading;
using System.Xml.Linq;

namespace AmestrisLauncher
{
    public partial class Extras : UserControl
    {
        private readonly Dictionary<string, string> Options = new Dictionary<string, string>();

        public Extras()
        {
            //Init
            this.DoubleBuffered = true;
            InitializeComponent();

            //Button Settings
            nextPage.FlatStyle = FlatStyle.Flat;
            nextPage.FlatAppearance.BorderSize = 0;
            nextPage.FlatAppearance.BorderColor = Color.FromArgb(0, 255, 255, 255); //Set no Border

            //Buttons 
            installSavegame.FlatAppearance.MouseOverBackColor = Color.FromArgb(25, 255, 255, 255);
            installSavegame.FlatAppearance.MouseDownBackColor = Color.FromArgb(75, 0, 255, 0); 
            installVoice.FlatAppearance.MouseOverBackColor = Color.FromArgb(25, 255, 255, 255);
            installVoice.FlatAppearance.MouseDownBackColor = Color.FromArgb(75, 0, 255, 0);
            resetButton.FlatAppearance.MouseOverBackColor = Color.FromArgb(25, 255, 255, 255);
            resetButton.FlatAppearance.MouseDownBackColor = Color.FromArgb(75, 0, 255, 0);
            nextPage.FlatAppearance.MouseOverBackColor = Color.FromArgb(15, 255, 255, 255);


            //Parse Settings, because we need it for the URI later on
            string settings = AppDomain.CurrentDomain.BaseDirectory + "\\Launcher.xml";
            if (File.Exists(settings))
            {
                try
                {
                    foreach (XElement el in XElement.Load(File.OpenRead(settings)).Elements())
                    {
                        Options.Add(el.Attribute("key").Value, el.Attribute("value").Value);
                    }
                }
                catch { ReportError("Ein Fehler ist während dem übernehmen der Einstellungen aufgetreten!"); return; }
            }

            //Check if Savegame has been Installed before
            RegistryKey savegamekey = Registry.CurrentUser.OpenSubKey("Amestris", true);
            if (savegamekey.GetValue("isSavegameInstalled") != null)
            {
                ThreadSafe(() => { installSavegame.Text = "Erneut Installieren"; });
            }

            //Check if Voice Plugin has been Installed before
            RegistryKey pluginkey = Registry.CurrentUser.OpenSubKey("Amestris", true);
            if (pluginkey.GetValue("isPluginInstalled") != null)
            {
                ThreadSafe(() => { installVoice.Text = "Erneut Installieren"; });
            }
        }

        //Install Savegame
        private void InstallSavegame_Click(object sender, System.EventArgs e)
        {
            FolderBrowserDialog savegameChoice = new FolderBrowserDialog();
            if (savegameChoice.ShowDialog() == DialogResult.OK)
            {
                string svgpathChoice = savegameChoice.SelectedPath;
                if (File.Exists(svgpathChoice + "\\pc_settings.bin"))
                {
                    //Disable other Buttons (Failsafe, because the Assets Directory will only get created, when its needed
                    ThreadSafe(() => { installSavegame.Enabled = false; });
                    ThreadSafe(() => { installVoice.Enabled = false; });
                    ThreadSafe(() => { installSavegame.Text = "Installiert ..."; });

                    //Define assets directory
                    string assetsdir = AppDomain.CurrentDomain.BaseDirectory + "\\assets\\";

                    //Delete if assets have been downloaded before
                    if (Directory.Exists(assetsdir))
                    {
                        Directory.Delete(assetsdir, true);
                    }

                    //Create Directory
                    DirectoryInfo di = Directory.CreateDirectory(assetsdir);

                    //Get current Savegame from Webspace
                    WebClient Client = new WebClient();
                    Client.DownloadFile(Options["SVG-DL"], assetsdir + "svg.zip");
                    Client.Dispose();

                    //Set filter to only delete SGTA Files and delete those
                    string filter = "SGTA";
                    string[] Files = Directory.GetFiles(svgpathChoice);
                    foreach (string file in Files)
                    {
                        if (file.ToUpper().Contains(filter.ToUpper()))
                        {
                            File.Delete(file);
                        }
                    }
                    //Unzip Savegame into selected Path
                    System.IO.Compression.ZipFile.ExtractToDirectory(assetsdir + "svg.zip", svgpathChoice);

                    //Force Delete Assets Directory at the end
                    di.Delete(true);

                    //Set successfull Registry Value
                    RegistryKey key = Registry.CurrentUser.OpenSubKey("Amestris", true);
                    key.SetValue("isSavegameInstalled", "true");
                    key.Close();

                    //Activate Buttons again
                    ThreadSafe(() => { installSavegame.Enabled = true; });
                    ThreadSafe(() => { installVoice.Enabled = true; });
                    ThreadSafe(() => { installSavegame.Text = "Erneut Installieren"; });
                }
                else
                {
                    ReportError("Der angegebene Pfad war leider nicht korrekt!\nBitte versuche es erneut!");
                }
            } 
        }

        //Install Plugin
        private void InstallVoice_Click(object sender, EventArgs e)
        {
            //Disable other Buttons (Failsafe, because the Assets Directory will only get created, when its needed
            ThreadSafe(() => { installSavegame.Enabled = false; });
            ThreadSafe(() => { installVoice.Enabled = false; });
            ThreadSafe(() => { installVoice.Text = "Installiert ..."; });

            //Define assets directory
            string assetsdir = AppDomain.CurrentDomain.BaseDirectory + "\\assets\\";

            //Delete if assets have been downloaded before
            if (Directory.Exists(assetsdir))
            {
                Directory.Delete(assetsdir, true);
            }

            //Create Directory
            DirectoryInfo di = Directory.CreateDirectory(assetsdir);

            //Get current Plugin from Webspace
            WebClient Client = new WebClient();
            Client.DownloadFile(Options["PLG-DL"], assetsdir + "plugin.zip");
            Client.Dispose();

            //Unzip Plugin into selected Path
            System.IO.Compression.ZipFile.ExtractToDirectory(assetsdir + "plugin.zip", assetsdir);

            //Execute Plugin exe
            Process ts3pluginInstaller = Process.Start(assetsdir + "gta5voice.ts3_plugin");

            //Delete Assets directory
            Thread.Sleep(8000); //Wait 8 Seconds for the User to accept running TS3 Plugin Installer as Administrator -> Otherwise it gets deleted too fast
            di.Delete(true);

            //Set successfull Registry Value
            RegistryKey key = Registry.CurrentUser.OpenSubKey("Amestris", true);
            key.SetValue("isPluginInstalled", "true");
            key.Close();

            //Activate Buttons again
            ThreadSafe(() => { installSavegame.Enabled = true; });
            ThreadSafe(() => { installVoice.Enabled = true; });
            ThreadSafe(() => { installVoice.Text = "Erneut Installieren"; });
        }

        //Method against Thread Error Message
        public static void ThreadSafe(Action act)
        {
            Dispatcher dispUI = Dispatcher.CurrentDispatcher;
            dispUI.BeginInvoke(act);
        }

        private static void ReportError(string msg)
        {
            MessageBox.Show(msg, "Fehler!",
                MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void ResetButton_Click(object sender, EventArgs e)
        {
            using (RegistryKey key = Registry.CurrentUser.OpenSubKey("Amestris", true))
            {
                if (key == null)
                {
                    ReportError("Du hast deine Daten schon gelöscht!\nStarte den Launcher neu!");
                }
                else
                {
                    Registry.CurrentUser.DeleteSubKey("Amestris");
                    Application.Exit();
                }
            }
        }

        private void NextPage_Click(object sender, EventArgs e)
        {
            ThreadSafe(() => { Control[] Co = TopLevelControl.Controls.Find("extrasSecond1", true); Co[0].Show(); });
            ThreadSafe(() => { Control[] Co = TopLevelControl.Controls.Find("extrasSecond1", true); Co[0].BringToFront(); });
        }
    }
}
