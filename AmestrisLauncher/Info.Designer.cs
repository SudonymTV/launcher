﻿namespace AmestrisLauncher
{
    partial class Info
    {
        /// <summary> 
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Komponenten-Designer generierter Code

        /// <summary> 
        /// Erforderliche Methode für die Designerunterstützung. 
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Info));
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.sudoLink = new System.Windows.Forms.LinkLabel();
            this.santiLink = new System.Windows.Forms.LinkLabel();
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Bahnschrift SemiBold", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(-90, 121);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(883, 237);
            this.label2.TabIndex = 2;
            this.label2.Text = resources.GetString("label2.Text");
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Bahnschrift", 28F, System.Drawing.FontStyle.Bold);
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(-74, 35);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(867, 46);
            this.label3.TabIndex = 6;
            this.label3.Text = "INFOS";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // sudoLink
            // 
            this.sudoLink.ActiveLinkColor = System.Drawing.Color.Transparent;
            this.sudoLink.AutoSize = true;
            this.sudoLink.BackColor = System.Drawing.Color.Transparent;
            this.sudoLink.DisabledLinkColor = System.Drawing.Color.Transparent;
            this.sudoLink.Font = new System.Drawing.Font("Bahnschrift SemiBold", 14F, System.Drawing.FontStyle.Bold);
            this.sudoLink.LinkColor = System.Drawing.Color.LightBlue;
            this.sudoLink.Location = new System.Drawing.Point(563, 123);
            this.sudoLink.Name = "sudoLink";
            this.sudoLink.Size = new System.Drawing.Size(89, 23);
            this.sudoLink.TabIndex = 7;
            this.sudoLink.TabStop = true;
            this.sudoLink.Text = "Sudonym";
            this.sudoLink.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.SudoLink_LinkClicked_1);
            // 
            // santiLink
            // 
            this.santiLink.ActiveLinkColor = System.Drawing.Color.Transparent;
            this.santiLink.AutoSize = true;
            this.santiLink.BackColor = System.Drawing.Color.Transparent;
            this.santiLink.DisabledLinkColor = System.Drawing.Color.Transparent;
            this.santiLink.Font = new System.Drawing.Font("Bahnschrift SemiBold", 14F, System.Drawing.FontStyle.Bold);
            this.santiLink.LinkColor = System.Drawing.Color.LightBlue;
            this.santiLink.Location = new System.Drawing.Point(517, 145);
            this.santiLink.Name = "santiLink";
            this.santiLink.Size = new System.Drawing.Size(84, 23);
            this.santiLink.TabIndex = 8;
            this.santiLink.TabStop = true;
            this.santiLink.Text = "Santiago";
            this.santiLink.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.SantiLink_LinkClicked);
            // 
            // linkLabel1
            // 
            this.linkLabel1.ActiveLinkColor = System.Drawing.Color.Transparent;
            this.linkLabel1.AutoSize = true;
            this.linkLabel1.BackColor = System.Drawing.Color.Transparent;
            this.linkLabel1.DisabledLinkColor = System.Drawing.Color.Transparent;
            this.linkLabel1.Font = new System.Drawing.Font("Bahnschrift SemiBold", 14F, System.Drawing.FontStyle.Bold);
            this.linkLabel1.LinkColor = System.Drawing.Color.LightBlue;
            this.linkLabel1.Location = new System.Drawing.Point(530, 169);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new System.Drawing.Size(65, 23);
            this.linkLabel1.TabIndex = 9;
            this.linkLabel1.TabStop = true;
            this.linkLabel1.Text = "Alenta";
            this.linkLabel1.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.LinkLabel1_LinkClicked);
            // 
            // Info
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(25)))), ((int)(((byte)(25)))));
            this.Controls.Add(this.sudoLink);
            this.Controls.Add(this.santiLink);
            this.Controls.Add(this.linkLabel1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.ForeColor = System.Drawing.Color.White;
            this.Name = "Info";
            this.Size = new System.Drawing.Size(886, 594);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.LinkLabel sudoLink;
        private System.Windows.Forms.LinkLabel santiLink;
        private System.Windows.Forms.LinkLabel linkLabel1;
    }
}
