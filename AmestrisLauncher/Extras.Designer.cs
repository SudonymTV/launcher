﻿namespace AmestrisLauncher
{
    partial class Extras
    {
        /// <summary> 
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Komponenten-Designer generierter Code

        /// <summary> 
        /// Erforderliche Methode für die Designerunterstützung. 
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.installSavegame = new System.Windows.Forms.Button();
            this.installVoice = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.resetButton = new System.Windows.Forms.Button();
            this.nextPage = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Bahnschrift", 18F, System.Drawing.FontStyle.Bold);
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(-84, 97);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(873, 31);
            this.label2.TabIndex = 5;
            this.label2.Text = "GTA V - 100% Savegame";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Bahnschrift SemiBold", 14.25F, System.Drawing.FontStyle.Bold);
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(-84, 131);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(873, 49);
            this.label1.TabIndex = 6;
            this.label1.Text = "Das 100% Savegame ist ein essentieller Teil der Installation.\r\nDieses wirst du br" +
    "auchen um bei diversen Toren/Türen o. Ä. durch zu kommen.";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Bahnschrift SemiBold", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(-81, 110);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(873, 27);
            this.label3.TabIndex = 7;
            this.label3.Text = "___________________________________________";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Bahnschrift", 18F, System.Drawing.FontStyle.Bold);
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(-84, 269);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(873, 29);
            this.label4.TabIndex = 8;
            this.label4.Text = "GTA V - Voice Plugin";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label5
            // 
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Bahnschrift SemiBold", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(-84, 282);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(873, 23);
            this.label5.TabIndex = 10;
            this.label5.Text = "_______________________________________";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label6
            // 
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Bahnschrift SemiBold", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.White;
            this.label6.Location = new System.Drawing.Point(-84, 307);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(873, 46);
            this.label6.TabIndex = 9;
            this.label6.Text = "Das Voice-Plugin ist u. A. auch ein essentieller Teil der Installation.\r\nDies wir" +
    "d verwendet um einen 3D-Audiobereich zu simulieren.\r\n";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label7
            // 
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Bahnschrift", 28F, System.Drawing.FontStyle.Bold);
            this.label7.ForeColor = System.Drawing.Color.White;
            this.label7.Location = new System.Drawing.Point(-96, 35);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(878, 46);
            this.label7.TabIndex = 11;
            this.label7.Text = "EXTRAS";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // installSavegame
            // 
            this.installSavegame.BackColor = System.Drawing.Color.Transparent;
            this.installSavegame.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Red;
            this.installSavegame.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.installSavegame.Font = new System.Drawing.Font("Bahnschrift SemiBold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.installSavegame.ForeColor = System.Drawing.Color.White;
            this.installSavegame.Location = new System.Drawing.Point(269, 220);
            this.installSavegame.Name = "installSavegame";
            this.installSavegame.Size = new System.Drawing.Size(152, 30);
            this.installSavegame.TabIndex = 12;
            this.installSavegame.TabStop = false;
            this.installSavegame.Text = "Installieren";
            this.installSavegame.UseVisualStyleBackColor = false;
            this.installSavegame.Click += new System.EventHandler(this.InstallSavegame_Click);
            // 
            // installVoice
            // 
            this.installVoice.BackColor = System.Drawing.Color.Transparent;
            this.installVoice.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.installVoice.Font = new System.Drawing.Font("Bahnschrift SemiBold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.installVoice.ForeColor = System.Drawing.Color.White;
            this.installVoice.Location = new System.Drawing.Point(269, 359);
            this.installVoice.Name = "installVoice";
            this.installVoice.Size = new System.Drawing.Size(152, 30);
            this.installVoice.TabIndex = 13;
            this.installVoice.TabStop = false;
            this.installVoice.Text = "Installieren";
            this.installVoice.UseVisualStyleBackColor = false;
            this.installVoice.Click += new System.EventHandler(this.InstallVoice_Click);
            // 
            // label9
            // 
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Bahnschrift", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.White;
            this.label9.Location = new System.Drawing.Point(-80, 181);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(873, 43);
            this.label9.TabIndex = 15;
            this.label9.Text = "Beispiel-Pfad: C:\\Users\\USERNAME\\Documents\\Rockstar Games\\GTA V\\Profiles\\XXXXXXX\r" +
    "\nInfo: Falls du zwei Ordner hast, kannst du auch in beide installieren.\r\n";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label8
            // 
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Bahnschrift", 18F, System.Drawing.FontStyle.Bold);
            this.label8.ForeColor = System.Drawing.Color.White;
            this.label8.Location = new System.Drawing.Point(-81, 405);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(873, 31);
            this.label8.TabIndex = 16;
            this.label8.Text = "Launcher zurücksetzen";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label10
            // 
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Bahnschrift SemiBold", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.White;
            this.label10.Location = new System.Drawing.Point(-78, 416);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(873, 27);
            this.label10.TabIndex = 18;
            this.label10.Text = "_______________________________________";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label11
            // 
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Bahnschrift SemiBold", 14.25F, System.Drawing.FontStyle.Bold);
            this.label11.ForeColor = System.Drawing.Color.White;
            this.label11.Location = new System.Drawing.Point(-81, 439);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(873, 49);
            this.label11.TabIndex = 17;
            this.label11.Text = "Wenn du den Launcher zurücksetzt, werden alle Daten gelöscht.\r\nDann kannst du z.B" +
    ". deinen RageMP Pfad neu angeben.";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // resetButton
            // 
            this.resetButton.BackColor = System.Drawing.Color.Transparent;
            this.resetButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.resetButton.Font = new System.Drawing.Font("Bahnschrift SemiBold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.resetButton.ForeColor = System.Drawing.Color.White;
            this.resetButton.Location = new System.Drawing.Point(269, 491);
            this.resetButton.Name = "resetButton";
            this.resetButton.Size = new System.Drawing.Size(152, 30);
            this.resetButton.TabIndex = 19;
            this.resetButton.TabStop = false;
            this.resetButton.Text = "Zurücksetzen";
            this.resetButton.UseVisualStyleBackColor = false;
            this.resetButton.Click += new System.EventHandler(this.ResetButton_Click);
            // 
            // nextPage
            // 
            this.nextPage.BackColor = System.Drawing.Color.Transparent;
            this.nextPage.BackgroundImage = global::AmestrisLauncher.Properties.Resources.arrow;
            this.nextPage.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.nextPage.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.nextPage.Font = new System.Drawing.Font("Bahnschrift SemiBold", 35F, System.Drawing.FontStyle.Bold);
            this.nextPage.ForeColor = System.Drawing.Color.Transparent;
            this.nextPage.Location = new System.Drawing.Point(685, 504);
            this.nextPage.Name = "nextPage";
            this.nextPage.Size = new System.Drawing.Size(51, 52);
            this.nextPage.TabIndex = 20;
            this.nextPage.TabStop = false;
            this.nextPage.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.nextPage.UseVisualStyleBackColor = false;
            this.nextPage.Click += new System.EventHandler(this.NextPage_Click);
            // 
            // Extras
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(25)))), ((int)(((byte)(25)))));
            this.Controls.Add(this.nextPage);
            this.Controls.Add(this.resetButton);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.installSavegame);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.installVoice);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label6);
            this.Name = "Extras";
            this.Size = new System.Drawing.Size(878, 565);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button installSavegame;
        private System.Windows.Forms.Button installVoice;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button resetButton;
        private System.Windows.Forms.Button nextPage;
    }
}
