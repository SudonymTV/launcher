﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Windows;
using System.Windows.Forms;
using System.Xml.Linq;

namespace AmestrisLauncher
{
    public partial class PatchNotes : UserControl
    {
        readonly Dictionary<string, string> Options = new Dictionary<string, string>();
        public PatchNotes()
        {
            InitializeComponent();
			this.DoubleBuffered = true;

            var settings = AppDomain.CurrentDomain.BaseDirectory + "\\Launcher.xml";
            if (File.Exists(settings))
            {
                try
                {
                    foreach (var el in XElement.Load(File.OpenRead(settings)).Elements())
                    {
                        Options.Add(el.Attribute("key").Value, el.Attribute("value").Value);
                    }
                }
                catch { ReportError("Ein Fehler ist während dem übernehmen der Einstellungen aufgetreten!"); return; }
            }
            else
            {
                ReportError("Kann 'Launcher.xml' nicht finden!");
                return;
            }

            //Configure WebBrowser for Patchnotes Page
            string url = Options["patchNotes"];

            if (url != null) { 
                this.patchBrowser.Navigate(url);
            } else
            {
                this.patchBrowser.Navigate("https://amestrisrp.com/forum/index.php?thread/1013-changelogs-am-05-07-2019-um-01-15-uhr/");
            }
        }

        //Disable Scrollbars, but leave scrolling via Mousewheel and Vertical Scrollbar enabled
        void PatchBrowser_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            patchBrowser.Document.Body.Style = "overflow-y: hidden; overflow-x: hidden; background: url('https://i.imgur.com/EO32GWM.jpg'); background-position: bottom;";
            patchBrowser.Document.GetElementById("main").Style += "padding-top: 10px;";

            Hide(patchBrowser.Document.GetElementById("pageHeaderContainer"));
            Hide(GetByClass("div", "pageHeaderContainer"));
            Hide(GetByClass("div", "boxesContentTop"));
            Hide(GetByClass("aside", "sidebar boxesSidebarRight"));
            Hide(GetByClass("div", "pageNavigation"));
            Hide(GetByClass("footer", "pageFooter"));
        }

        private HtmlElement GetByClass(string tagName, string className)
        {
            var elements = patchBrowser.Document.GetElementsByTagName(tagName);
            foreach (HtmlElement elem in elements)
            {
                if (elem.GetAttribute("className") != className) continue;
                return elem;
            }

            return null;
        }

        private void Hide(HtmlElement element)
        {
            if (element == null) return;
            element.Style = $"{element.Style} display: none;";
        }

        private static void ReportError(String msg)
        {
            MessageBox.Show(msg, "Fehler!",
                MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
    }
}
