﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Windows.Forms;
using System.Windows.Threading;
using System.Xml.Linq;

namespace AmestrisLauncher
{
    public partial class ExtrasThird : UserControl
    {
        private readonly Dictionary<string, string> Options = new Dictionary<string, string>();

        public ExtrasThird()
        {
            InitializeComponent();
            DoubleBuffered = true;
            CheckForButtons();

            //Buttons 
            installRoads.FlatAppearance.MouseOverBackColor = Color.FromArgb(25, 255, 255, 255);
            installRoads.FlatAppearance.MouseDownBackColor = Color.FromArgb(75, 0, 255, 0);
            uninstallRoads.FlatAppearance.MouseOverBackColor = Color.FromArgb(25, 255, 255, 255);
            uninstallRoads.FlatAppearance.MouseDownBackColor = Color.FromArgb(75, 0, 255, 0);
        }

        private void InstallRoads_Click(object sender, EventArgs e)
        {
            //Inform User about Application Hang before starting
            ReportInfo("Während der Installation ist der Launcher nicht verwendbar!\n\nBitte schließe den Launcher nicht!");

            Process[] pname = Process.GetProcessesByName("gta5");
            if (pname.Length == 0)
            {
                //Parse Settings, because we need it for the URI later on
                string settings = AppDomain.CurrentDomain.BaseDirectory + "\\Launcher.xml";
                if (File.Exists(settings))
                {
                    try
                    {
                        foreach (XElement el in XElement.Load(File.OpenRead(settings)).Elements())
                        {
                            Options.Add(el.Attribute("key").Value, el.Attribute("value").Value);
                        }
                    }
                    catch
                    {
                        ReportError("Du musst den Launcher zuvor neustarten!");
                        CheckForButtons();
                        return;
                    }
                }

                //Set installed Registry Value (moved up here, to be able to activate deinstallation button)
                RegistryKey key = Registry.CurrentUser.OpenSubKey("Amestris", true);
                key.SetValue("isRoadsInstalled", "true");
                key.Close();

                //Get GTAV Path from RageMP Registry
                RegistryKey amestrisReg = Registry.CurrentUser.OpenSubKey(@"Amestris", false);
                if (amestrisReg.GetValueNames().Contains("RageMP-Path"))
                {
                    //Define GTA V Path - Get from Registry
                    string ragepath = amestrisReg.GetValue("RageMP-Path").ToString() + "\\";

                    //Define Client resources folder
                    string clrs = ragepath + "\\client_resources\\" + Options["CR-IP"] + "\\";

                    //Define assets directory
                    string assetsdir = AppDomain.CurrentDomain.BaseDirectory + "\\assets\\";

                    //Delete if assets have been downloaded before
                    if (Directory.Exists(assetsdir))
                    {
                        Directory.Delete(assetsdir, true);
                    }

                    //Create Assets Directory
                    DirectoryInfo assetsdi = Directory.CreateDirectory(assetsdir);

                    //Get current LAR Version from Webspace
                    WebClient Client = new WebClient();
                    Client.DownloadFile(Options["LAR-DL"], assetsdir + "lar.zip");
                    Client.Dispose();

                    //Unzip LAR into clrs dlcpacks
                    System.IO.Compression.ZipFile.ExtractToDirectory(assetsdir + "lar.zip", clrs + "dlcpacks\\");

                    //Delete Assets directory
                    assetsdi.Delete(true);

                    //Activate Buttons again
                    CheckForButtons();

                    ReportInfo("Installation erfolgreich abgeschlossen!");
                }
                else
                {
                    ReportError("Wir konnten deinen RageMP Installationspfad nicht abrufen!\n\nBitte gebe im Updates Tab deinen Game Pfad an!");
                    CheckForButtons();
                }
            }
            else
            {
                ReportError("Dein GTA V läuft noch!\n\nBitte beende es vor der Installation!");
                CheckForButtons();
            }
        }

        private void UninstallRoads_Click(object sender, EventArgs e)
        {
            Process[] pname = Process.GetProcessesByName("gta5");
            if (pname.Length == 0)
            {
                //Parse Settings, because we need it for the URI later on
                string settings = AppDomain.CurrentDomain.BaseDirectory + "\\Launcher.xml";
                if (File.Exists(settings))
                {
                    try
                    {
                        foreach (XElement el in XElement.Load(File.OpenRead(settings)).Elements())
                        {
                            Options.Add(el.Attribute("key").Value, el.Attribute("value").Value);
                        }
                    }
                    catch
                    {
                        ReportError("Du musst den Launcher zuvor neustarten!");
                        CheckForButtons();
                        return;
                    }
                }

                //Get GTAV Path from RageMP Registry
                RegistryKey amestrisReg = Registry.CurrentUser.OpenSubKey(@"Amestris", false);
                if (amestrisReg.GetValueNames().Contains("RageMP-Path"))
                {
                    //Define GTA V Path - Get from Registry
                    string ragepath = amestrisReg.GetValue("RageMP-Path").ToString() + "\\";

                    //Define Client resources folder
                    string clrs = ragepath + "client_resources\\" + Options["CR-IP"] + "\\";

                    //Delete LAR from Client Resources
                    if (Directory.Exists(clrs + "dlcpacks\\LA_ROADS"))
                    {
                        Directory.Delete(clrs + "dlcpacks\\LA_ROADS", true);
                    }

                    //Delete registry value
                    RegistryKey key = Registry.CurrentUser.OpenSubKey("Amestris", true);
                    key.DeleteValue("isRoadsInstalled");
                    key.Close();

                    //Activate Buttons again
                    CheckForButtons();

                    //Set Button Text for Installer
                    ThreadSafe(() => { installRoads.Text = "Installieren"; });
                }
                else
                {
                    ReportError("Wir konnten deinen RageMP Installationspfad nicht abrufen!\n\nBitte gebe im Updates Tab deinen Game Pfad an!");
                    CheckForButtons();
                }
            }
            else
            {
                ReportError("Dein GTA V läuft noch!\n\nBitte beende es vor der Installation!");
                CheckForButtons();
            }
        }

        private static void ReportError(String msg)
        {
            MessageBox.Show(msg, "Fehler!",
                MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private static void ReportInfo(String msg)
        {
            MessageBox.Show(msg, "Info!",
                MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
        public static void ThreadSafe(Action act)
        {
            Dispatcher dispUI = Dispatcher.CurrentDispatcher;
            dispUI.BeginInvoke(act);
        }

        public void CheckForButtons()
        {
            RegistryKey amestrisReg = Registry.CurrentUser.OpenSubKey(@"Amestris", false);
            if (amestrisReg.GetValueNames().Contains("isRoadsInstalled"))
            {
                ThreadSafe(() => { installRoads.Enabled = false; });
                ThreadSafe(() => { uninstallRoads.Enabled = true; });
                ThreadSafe(() => { installRoads.Text = "Bereits installiert!"; });
            }
            else
            {
                ThreadSafe(() => { installRoads.Enabled = true; });
                ThreadSafe(() => { uninstallRoads.Enabled = false; });
            }
        }
    }
}
