﻿namespace AmestrisLauncher
{
    partial class ExtrasSecond
    {
        /// <summary> 
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Komponenten-Designer generierter Code

        /// <summary> 
        /// Erforderliche Methode für die Designerunterstützung. 
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ExtrasSecond));
            this.installRedux = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.uninstallRedux = new System.Windows.Forms.Button();
            this.uninstallNVR = new System.Windows.Forms.Button();
            this.installNVR = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.nextPage = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // installRedux
            // 
            this.installRedux.BackColor = System.Drawing.Color.Transparent;
            this.installRedux.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.installRedux.Font = new System.Drawing.Font("Bahnschrift SemiBold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.installRedux.ForeColor = System.Drawing.Color.White;
            this.installRedux.Location = new System.Drawing.Point(211, 248);
            this.installRedux.Name = "installRedux";
            this.installRedux.Size = new System.Drawing.Size(138, 51);
            this.installRedux.TabIndex = 20;
            this.installRedux.TabStop = false;
            this.installRedux.Text = "Installieren";
            this.installRedux.UseVisualStyleBackColor = false;
            this.installRedux.Click += new System.EventHandler(this.InstallRedux_Click);
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Bahnschrift", 18F, System.Drawing.FontStyle.Bold);
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(-77, 106);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(859, 31);
            this.label2.TabIndex = 16;
            this.label2.Text = "Modifikation - Redux";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Bahnschrift SemiBold", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(-74, 119);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(859, 27);
            this.label3.TabIndex = 18;
            this.label3.Text = "___________________________________________";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Bahnschrift SemiBold", 14.25F, System.Drawing.FontStyle.Bold);
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(-77, 140);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(859, 49);
            this.label1.TabIndex = 17;
            this.label1.Text = "Redux ist eine Mod, die das Spielverhalten, sowie das Aussehen verändert.\r\nDarunt" +
    "er zählt u.A. die Grafik, Sounds, Lichtverhältnisse uvm.";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label7
            // 
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Bahnschrift", 28F, System.Drawing.FontStyle.Bold);
            this.label7.ForeColor = System.Drawing.Color.White;
            this.label7.Location = new System.Drawing.Point(-89, 35);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(864, 46);
            this.label7.TabIndex = 19;
            this.label7.Text = "EXTRAS";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label9
            // 
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Bahnschrift", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.White;
            this.label9.Location = new System.Drawing.Point(-73, 180);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(873, 72);
            this.label9.TabIndex = 21;
            this.label9.Text = resources.GetString("label9.Text");
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // uninstallRedux
            // 
            this.uninstallRedux.BackColor = System.Drawing.Color.Transparent;
            this.uninstallRedux.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.uninstallRedux.Font = new System.Drawing.Font("Bahnschrift SemiBold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uninstallRedux.ForeColor = System.Drawing.Color.White;
            this.uninstallRedux.Location = new System.Drawing.Point(355, 248);
            this.uninstallRedux.Name = "uninstallRedux";
            this.uninstallRedux.Size = new System.Drawing.Size(138, 51);
            this.uninstallRedux.TabIndex = 22;
            this.uninstallRedux.TabStop = false;
            this.uninstallRedux.Text = "Deinstallieren";
            this.uninstallRedux.UseVisualStyleBackColor = false;
            this.uninstallRedux.Click += new System.EventHandler(this.UninstallRedux_Click);
            // 
            // uninstallNVR
            // 
            this.uninstallNVR.BackColor = System.Drawing.Color.Transparent;
            this.uninstallNVR.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.uninstallNVR.Font = new System.Drawing.Font("Bahnschrift SemiBold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uninstallNVR.ForeColor = System.Drawing.Color.White;
            this.uninstallNVR.Location = new System.Drawing.Point(352, 478);
            this.uninstallNVR.Name = "uninstallNVR";
            this.uninstallNVR.Size = new System.Drawing.Size(138, 51);
            this.uninstallNVR.TabIndex = 28;
            this.uninstallNVR.TabStop = false;
            this.uninstallNVR.Text = "Deinstallieren";
            this.uninstallNVR.UseVisualStyleBackColor = false;
            this.uninstallNVR.Click += new System.EventHandler(this.UninstallNVR_Click);
            // 
            // installNVR
            // 
            this.installNVR.BackColor = System.Drawing.Color.Transparent;
            this.installNVR.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.installNVR.Font = new System.Drawing.Font("Bahnschrift SemiBold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.installNVR.ForeColor = System.Drawing.Color.White;
            this.installNVR.Location = new System.Drawing.Point(208, 478);
            this.installNVR.Name = "installNVR";
            this.installNVR.Size = new System.Drawing.Size(138, 51);
            this.installNVR.TabIndex = 26;
            this.installNVR.TabStop = false;
            this.installNVR.Text = "Installieren";
            this.installNVR.UseVisualStyleBackColor = false;
            this.installNVR.Click += new System.EventHandler(this.InstallNVR_Click);
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Bahnschrift", 18F, System.Drawing.FontStyle.Bold);
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(-80, 336);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(859, 31);
            this.label4.TabIndex = 23;
            this.label4.Text = "Modifikation - NVR";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label5
            // 
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Bahnschrift SemiBold", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(-77, 349);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(859, 27);
            this.label5.TabIndex = 25;
            this.label5.Text = "___________________________________________";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label6
            // 
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Bahnschrift SemiBold", 14.25F, System.Drawing.FontStyle.Bold);
            this.label6.ForeColor = System.Drawing.Color.White;
            this.label6.Location = new System.Drawing.Point(-80, 370);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(859, 49);
            this.label6.TabIndex = 24;
            this.label6.Text = "NVR (Natural Vision Remastered) ist ähnlich wie Redux.\r\nAuch diese Mod verändert " +
    "Grafik, Lichtverhältnisse uvm.";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label8
            // 
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Bahnschrift", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.White;
            this.label8.Location = new System.Drawing.Point(-76, 410);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(873, 72);
            this.label8.TabIndex = 27;
            this.label8.Text = resources.GetString("label8.Text");
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // nextPage
            // 
            this.nextPage.BackColor = System.Drawing.Color.Transparent;
            this.nextPage.BackgroundImage = global::AmestrisLauncher.Properties.Resources.arrow;
            this.nextPage.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.nextPage.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.nextPage.Font = new System.Drawing.Font("Bahnschrift SemiBold", 35F, System.Drawing.FontStyle.Bold);
            this.nextPage.ForeColor = System.Drawing.Color.Transparent;
            this.nextPage.Location = new System.Drawing.Point(685, 504);
            this.nextPage.Name = "nextPage";
            this.nextPage.Size = new System.Drawing.Size(51, 52);
            this.nextPage.TabIndex = 29;
            this.nextPage.TabStop = false;
            this.nextPage.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.nextPage.UseVisualStyleBackColor = false;
            this.nextPage.Click += new System.EventHandler(this.NextPage_Click);
            // 
            // ExtrasSecond
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(75)))), ((int)(((byte)(75)))), ((int)(((byte)(75)))), ((int)(((byte)(75)))));
            this.Controls.Add(this.nextPage);
            this.Controls.Add(this.uninstallNVR);
            this.Controls.Add(this.installNVR);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.uninstallRedux);
            this.Controls.Add(this.installRedux);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label9);
            this.Name = "ExtrasSecond";
            this.Size = new System.Drawing.Size(915, 627);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button installRedux;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button uninstallRedux;
        private System.Windows.Forms.Button uninstallNVR;
        private System.Windows.Forms.Button installNVR;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button nextPage;
    }
}
