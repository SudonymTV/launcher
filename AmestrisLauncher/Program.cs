﻿using System;
using System.IO;
using System.Windows.Forms;
using System.Xml.Linq;
using System.Net.Http;
using Microsoft.Win32;

namespace AmestrisLauncher
{
    class Program
    {
        public static string IpAddress = null;

        //Make Application Single-Threaded
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            //Execute Anticheat and Announce at Application start
            //WebsocketHandler.AnnounceUser();
            //AntiCheat.ScanProcesses();

            Timer scanProc = new Timer
            {
                Interval = 10000 // Scan Proccesses every two Minutes
            };
            scanProc.Tick += new EventHandler(AnticheatTimer); 
            scanProc.Start();


            Timer announceTimer = new Timer()
            {
                Interval = 45000
            };
            announceTimer.Tick += new EventHandler(AnnounceUser);
            announceTimer.Start();

            //Get User Call
            /*
            Program.IpAddress = GetUser();
            if (string.IsNullOrWhiteSpace(Program.IpAddress))
            {
                MessageBox.Show("Deine IP-Adresse konnte nicht abgefragt werden\nDer Launcher schließt sich nun.");
                Environment.Exit(0);
            } */

            RegistryKey key = Registry.CurrentUser.OpenSubKey("Amestris", true);

            //If local Settings exist == AGB Accepted && IPAdress got loaded, and User was announced -> Run
            if (key.GetValue("AGB") != null)
            {
                Application.Run(new backgroundImage());
            } else
            {
                //Ask for AGB
                System.Diagnostics.Process.Start("https://amestrisrp.com/forum/index.php?thread/275-amestris-roleplay-der-launcher/");
                DialogResult dialogResult = MessageBox.Show("Um den Launcher verwenden zu können,\nmusst du vorerst unsere AGB akzeptieren!\n\nDie AGB haben sich soeben in deinem Browser geöffnet.\n\nAkzeptierst du diese?", "AGB - Amestris Launcher", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    RegistryKey setAGB = Registry.CurrentUser.OpenSubKey("Amestris", true);
                    setAGB.SetValue("AGB", "Accepted");
                    setAGB.Close();
                    Application.Run(new backgroundImage());
                }
                else if (dialogResult == DialogResult.No)
                {
                    Application.Exit();
                }
            }
        }

        public static void AnticheatTimer(object sender, EventArgs e)
        {
            //AntiCheat.ScanProcesses();
        }

        public static void AnnounceUser(object sender, EventArgs e)
        {
            //WebsocketHandler.AnnounceUser();
        }


        /* Get User external IP-Adress */
        private static string GetUser()
        {
            using (var client = new HttpClient())
            {
                try
                {
                    var response = client.GetStringAsync("http://ipinfo.io/ip").GetAwaiter().GetResult();
                    return response.Trim();
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                    return "";
                }
            }
        }
    }
}
