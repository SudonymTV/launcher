﻿using System.Windows.Forms;
using System.Diagnostics;

namespace AmestrisLauncher
{
    public partial class Info : UserControl
    {
        public Info()
        {
            InitializeComponent();
			this.DoubleBuffered = true;
        }

        private void SantiLink_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start("https://amestrisrp.com/index.php?user/134-marquezvt/");
        }

        private void SudoLink_LinkClicked_1(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start("https://amestrisrp.com/index.php?user/269-sudonym/");
        }

        private void LinkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start("https://amestrisrp.com/index.php?user/630-alenta/");
        }
    }
}
