﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Windows.Forms;
using System.Windows.Threading;
using System.Xml.Linq;

namespace AmestrisLauncher
{
    public partial class Update : UserControl
    {
        private readonly Dictionary<string, string> Options = new Dictionary<string, string>();
        static List<string> FileFilter = new List<string>();
        private readonly BackgroundWorker bw = new BackgroundWorker();

        public Update()
        {
            //Disable Button onStartUp (Before Update gets initialized, because checkIfLocal -> True -> Enable)
            ThreadSafe(() => { UpdateStart.Enabled = false; });

            //Button Design
            ThreadSafe(() => { UpdateStart.BackColor = Color.Transparent; });
            ThreadSafe(() => { UpdateStart.BackgroundImageLayout = ImageLayout.Stretch; });
            ThreadSafe(() => { UpdateStart.BackgroundImage = SetImageOpacity(Properties.Resources.buttonFade, 0.8F); });

            //Button Hover Event
            ThreadSafe(() => { UpdateStart.MouseEnter += new EventHandler(ButtonUpdate_MouseEnter); });
            ThreadSafe(() => { UpdateStart.MouseLeave += new EventHandler(ButtonUpdate_MouseLeave); });

            //Init Comp
            InitializeComponent();
            DoubleBuffered = true;

            metroProgressBar1.ForeColor = Color.FromArgb(77, 0, 77);
            metroProgressBar1.BackColor = Color.FromArgb(0, 0, 0);

            //BW
            bw.WorkerReportsProgress = true;
            bw.ProgressChanged += new ProgressChangedEventHandler(Bw_ProgressChanged);
            bw.RunWorkerCompleted += new RunWorkerCompletedEventHandler(Bw_RunWorkerCompleted);

            //Init update
            Initialize_Update();
            SetVersion();

            //Button-Update Settings
            UpdateStart.FlatStyle = FlatStyle.Flat;
            UpdateStart.FlatAppearance.BorderSize = 0;
            UpdateStart.FlatAppearance.BorderColor = Color.FromArgb(0, 255, 255, 255); //Set no Border

            //Button Select Path
            pathChoice.FlatAppearance.MouseOverBackColor = Color.FromArgb(15, 255, 255, 255);
            pathChoice.FlatAppearance.MouseDownBackColor = Color.FromArgb(75, 0, 255, 0);

            //Label Settings - Break Lines
            resultLabelNew.MaximumSize = new Size(750, 0);
            resultLabelNew.AutoSize = true;

            //Check if Path has been selected before -> Disable button if true
            RegistryKey key = Registry.CurrentUser.OpenSubKey("Amestris", true);
            if (key.GetValue("RageMP-Path") != null)
            {
                ThreadSafe(() => { pathChoice.Enabled = false; });
            } else
            {
                ThreadSafe(() => { pathChoice.Enabled = true; });
            }
        }

        private void ButtonUpdate_MouseEnter(object sender, EventArgs e)
        {
            UpdateStart.ForeColor = Color.GhostWhite;
        }
        private void ButtonUpdate_MouseLeave(object sender, EventArgs e)
        {
            UpdateStart.ForeColor = Color.Black;
        }

        //Method against Thread Error Message
        public static void ThreadSafe(Action act)
        {
            Dispatcher dispUI = Dispatcher.CurrentDispatcher;
            dispUI.BeginInvoke(act);
        }

        public void PathChoice_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog rageChoice = new FolderBrowserDialog();

            if (rageChoice.ShowDialog() == DialogResult.OK)
            {
                string tempdir = rageChoice.SelectedPath;
                if (File.Exists(tempdir + "\\ragemp_v.exe"))
                {
                    ThreadSafe(() => { resultLabelNew.Text = "Der Pfad war korrekt! - Du kannst das Update jetzt starten!"; });
                    ThreadSafe(() => { UpdateStart.Enabled = true; });

                    RegistryKey key = Registry.CurrentUser.OpenSubKey("Amestris", true);
                    key.SetValue("RageMP-Path", tempdir);
                    key.Close();
                }
                else
                {
                    ThreadSafe(() => { UpdateStart.Enabled = false; });
                    ThreadSafe(() => { resultLabelNew.Text = "Der Pfad war nicht korrekt! - Versuche es erneut!"; });
                }
            }
        }

        //Start Update onClick -> Prepare File List -> Parse into DoWork
        private void UpdateStart_Click(object sender, EventArgs e)
        {
            try
            {
                //Prepare Stuff for BW Async File Grab
                byte[] data = new WebClient().DownloadData(Options["CR-DL"] + "SETTINGS/Checksums.xml");
                XElement remotefiles = XElement.Load(new MemoryStream(data));
                metroProgressBar1.Maximum = remotefiles.Elements().Count();
                bw.RunWorkerAsync(remotefiles);
                bw.DoWork += new DoWorkEventHandler(Bw_DoWork);
                ThreadSafe(() => { Control[] Co = TopLevelControl.Controls.Find("ButtonPlay", true); Co[0].Enabled = false; });
                ThreadSafe(() => { UpdateStart.Enabled = false; });
                ThreadSafe(() => { resultLabelNew.Text = "Downloading / Checking ..."; });
            }
            catch { ReportError("Ein Fehler ist aufgetreten, während die Remote-Daten processed wurden!"); return; }
        }

        //Everything that has to do with Patching
        private void Bw_DoWork(object sender, DoWorkEventArgs e)
        {
            int current = 0;
            XElement files = e.Argument as XElement;

            try
            {
                foreach (XElement file in files.Elements())
                {
                    RegistryKey key = Registry.CurrentUser.OpenSubKey("Amestris", true);
                    string path = key.GetValue("RageMP-Path").ToString();
                    string ip = Options["CR-IP"];
                    string dir = path + "\\client_resources\\" + ip + "\\";
                    string name = file.Attribute("name").Value;
                    string rhash = file.Attribute("hash").Value;
                    FileFilter.Add(name); //Add Name to List -> is beeing used to delete unwanted files afterwards
                    bool download = false;

                    if (!File.Exists(dir + name))
                    {
                        download = true;
                    }
                    else
                    {
                        using (FileStream f = File.OpenRead(dir + name))
                        {
                            SHA256Managed sha = new SHA256Managed();
                            string lhash = BitConverter.ToString(sha.ComputeHash(f)).Replace("-", "");
                            if (!string.Equals(lhash, rhash))
                            {
                                download = true;
                            }
                        }
                    }

                    if (download)
                    {
                        ThreadSafe(() => { resultLabelNew.Text = ("Downloade " + name + "..."); });
                        if (!Directory.Exists(System.IO.Path.GetDirectoryName(dir + name)))
                        {
                            Directory.CreateDirectory(System.IO.Path.GetDirectoryName(dir + name));
                        }

                        if (File.Exists(dir + name))
                        {
                            File.Delete(dir + name);
                        }

                        using (WebClient client = new WebClient())
                        {
                            try
                            {
                                client.DownloadFile(Options["CR-DL"] + name, dir + name);
                            }
                            catch { ReportError(string.Format("Während des Downloads von '{0}' ist ein Fehler aufgetreten!", name)); return; }
                        }
                    }
                    current++;
                    bw.ReportProgress(current);
                }

                // Delete files which are not in the checksums.xml
                try
                {
                    //Get-Set Path
                    RegistryKey key = Registry.CurrentUser.OpenSubKey("Amestris", true);
                    string path = key.GetValue("RageMP-Path").ToString();
                    string ip = Options["CR-IP"];
                    string dir = path + "\\client_resources\\" + ip + "\\";
                    //Create Dir-Info Object
                    DirectoryInfo d = new DirectoryInfo(dir);
                    //Delete if not in fileFilter List
                    foreach (var file in Directory.GetFiles(dir, "*.*", SearchOption.AllDirectories))
                    {
                        FileInfo fileIndex = new FileInfo(file);
                        string fileName = fileIndex.FullName.Replace(dir, "");

                        bool debug = false;
                        if (!FileFilter.Contains(fileName))
                        {
                            if (fileName == ".storage")
                            {
                                if (debug)
                                {
                                    Console.WriteLine("[DELETION] Skipped Deletion of .storage");
                                }
                            }
                            else if (fileName == @"dlcpacks\LA_ROADS\dlc.rpf")
                            {
                                if (debug)
                                {
                                    Console.WriteLine("[DELETION] Skipped Deletion of LA-R");
                                }
                            } 
                            else
                            {
                                if (debug)
                                {
                                    Console.WriteLine("[DELETION] Deleting " + fileName + " ...");
                                }
                                Console.WriteLine("[EXECUTE] DELETE: " + file);
                                File.Delete(file); 
                            }
                        }
                    }
                }
                catch
                {
                    ReportError("Beim löschen von Dateien die nicht gewhitelisted sind ist ein Fehler aufgetreten!");
                }
            }
            catch (Exception error)
            {
                MessageBox.Show(error.ToString(), "Fehler!",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        //Gets called if Progress Changed
        private void Bw_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            ThreadSafe(() => { metroProgressBar1.Value = e.ProgressPercentage; });
        }

        //Gets called when worker is completed
        private void Bw_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            ThreadSafe(() => { UpdateStart.Enabled = true; });
            ThreadSafe(() => { Control[] Co = TopLevelControl.Controls.Find("buttonPlay", true); Co[0].Enabled = true; });
            ThreadSafe(() => { resultLabelNew.Text = "Deine Client-Files sind Up-To-Date!"; });
        }

        public void SetVersion()
        {
            //Set Current Version
            string localVersionPath = AppDomain.CurrentDomain.BaseDirectory + "\\LocalVersion.xml";
            if (File.Exists(localVersionPath))
            {
                XElement root = new XElement("options");
                XDocument doc = new XDocument(root);
                XElement element = new XElement("option");
                element.SetAttributeValue("key", "version");
<<<<<<< HEAD
                element.SetAttributeValue("value", "22");
=======
                element.SetAttributeValue("value", "21");
>>>>>>> master
                doc.Root.Add(element);
                doc.Save(localVersionPath);
            }
        }

        public void Initialize_Update()
        {
            //Load Settings
            string settings = AppDomain.CurrentDomain.BaseDirectory + "\\Launcher.xml";
            if (File.Exists(settings))
            {
                try
                {
                    foreach (XElement el in XElement.Load(File.OpenRead(settings)).Elements())
                    {
                        Options.Add(el.Attribute("key").Value, el.Attribute("value").Value);
                    }
                }
                catch { ReportError("Ein Fehler ist während dem übernehmen der Einstellungen aufgetreten!"); return; }
            }

            RegistryKey key1 = Registry.CurrentUser.OpenSubKey("Amestris", true);
            if (key1.GetValue("RageMP-Path") != null)
            {
                ThreadSafe(() => { pathChoice.Enabled = false; });
                ThreadSafe(() => { resultLabelNew.Text = "Der Pfad wurde gefunden! - Du kannst das Update jetzt starten!"; });
                ThreadSafe(() => { UpdateStart.Enabled = true; });
            }
            else
            {
                ThreadSafe(() => { pathChoice.Enabled = true; });
                ThreadSafe(() => { UpdateStart.Enabled = false; });
                ThreadSafe(() => { resultLabelNew.Text = "Warte auf RageMP Pfad ..."; });
            }
        }

        private static void ReportError(string msg)
        {
            MessageBox.Show(msg, "Fehler!",
                MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void CompareFiles(object sender, DoWorkEventArgs e)
        {
        }

        private void Update_Load(object sender, EventArgs e)
        {

        }

        public Image SetImageOpacity(Image image, float opacity)
        {
            Bitmap bmp = new Bitmap(image.Width, image.Height);
            using (Graphics g = Graphics.FromImage(bmp))
            {
                ColorMatrix matrix = new ColorMatrix
                {
                    Matrix33 = opacity
                };
                ImageAttributes attributes = new ImageAttributes();
                attributes.SetColorMatrix(matrix, ColorMatrixFlag.Default,
                                                  ColorAdjustType.Bitmap);
                g.DrawImage(image, new Rectangle(0, 0, bmp.Width, bmp.Height),
                                   0, 0, image.Width, image.Height,
                                   GraphicsUnit.Pixel, attributes);
            }
            return bmp;
        }
    }
}
