﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading;
using System.Windows.Forms;
using System.Windows.Threading;
using System.Xml.Linq;
using Microsoft.Win32;

namespace AmestrisLauncher
{
    public partial class ExtrasSecond : UserControl
    {
        private readonly Dictionary<string, string> Options = new Dictionary<string, string>();

        public ExtrasSecond()
        {
            InitializeComponent();
            DoubleBuffered = true;
            CheckForButtons();

            //Button Settings
            nextPage.FlatStyle = FlatStyle.Flat;
            nextPage.FlatAppearance.BorderSize = 0;
            nextPage.FlatAppearance.BorderColor = Color.FromArgb(0, 255, 255, 255); //Set no Border

            //Buttons 
            installRedux.FlatAppearance.MouseOverBackColor = Color.FromArgb(25, 255, 255, 255);
            installRedux.FlatAppearance.MouseDownBackColor = Color.FromArgb(75, 0, 255, 0);
            installNVR.FlatAppearance.MouseOverBackColor = Color.FromArgb(25, 255, 255, 255);
            installNVR.FlatAppearance.MouseDownBackColor = Color.FromArgb(75, 0, 255, 0);
            uninstallRedux.FlatAppearance.MouseOverBackColor = Color.FromArgb(25, 255, 255, 255);
            uninstallRedux.FlatAppearance.MouseDownBackColor = Color.FromArgb(75, 0, 255, 0);
            uninstallNVR.FlatAppearance.MouseOverBackColor = Color.FromArgb(25, 255, 255, 255);
            uninstallNVR.FlatAppearance.MouseDownBackColor = Color.FromArgb(75, 0, 255, 0);
            nextPage.FlatAppearance.MouseOverBackColor = Color.FromArgb(15, 255, 255, 255);
        }

        private void InstallRedux_Click(object sender, EventArgs e)
        {
            RegistryKey amestrisReg = Registry.CurrentUser.OpenSubKey(@"Amestris", false);
            if (!amestrisReg.GetValueNames().Contains("isReduxInstalled"))
            {
                //Disable Buttons and set Infotext (not via Threadsafe)
                installRedux.Enabled = false;
                uninstallRedux.Enabled = false;
                installNVR.Enabled = false;
                uninstallNVR.Enabled = false;
                installRedux.Text = "Installiert ...";

                //Inform User about Application Hang before starting
                ReportInfo("Während der Installation ist der Launcher nicht verwendbar!\n\nBitte schließe den Launcher nicht!");

                //Sleep for a short period of Time to avoid Thread miscommunication
                Thread.Sleep(2000);

                Process[] pname = Process.GetProcessesByName("gta5");
                if (pname.Length == 0)
                {
                    //Parse Settings, because we need it for the URI later on
                    string settings = AppDomain.CurrentDomain.BaseDirectory + "\\Launcher.xml";
                    if (File.Exists(settings))
                    {
                        try
                        {
                            foreach (XElement el in XElement.Load(File.OpenRead(settings)).Elements())
                            {
                                Options.Add(el.Attribute("key").Value, el.Attribute("value").Value);
                            }
                        }
                        catch
                        {
                            ReportError("Du musst den Launcher zuvor neustarten!");
                            CheckForButtons();
                            return;
                        }
                    }

                    //Set installed Registry Value (moved up here, to be able to activate deinstallation button)
                    RegistryKey key = Registry.CurrentUser.OpenSubKey("Amestris", true);
                    key.SetValue("isReduxInstalled", "true");
                    key.Close();

                    //Get GTAV Path from RageMP Registry
                    RegistryKey rageReg = Registry.CurrentUser.OpenSubKey(@"SOFTWARE\RAGE-MP", false);
                    if (rageReg.GetValueNames().Contains("game_v_path"))
                    {
                        try
                        {
                            //Define GTA Base Directory
                            string gtavpath = rageReg.GetValue("game_v_path").ToString() + "\\";

                            //Define backup directory
                            string backupdir = gtavpath + "backupamestris";

                            //Check if another Mod has been installed before and if a backup got created -> if true, move backup back to root directory
                            if (Directory.Exists(backupdir))
                            {
                                DirectoryInfo backupDirTemp = new DirectoryInfo(backupdir);
                                foreach (var file in backupDirTemp.GetFiles("*", SearchOption.AllDirectories))
                                {
                                    File.Move(backupdir + "\\" + file, gtavpath + file);
                                }

                                Directory.Delete(backupdir, true);
                            }

                            //Create Backup Directory
                            DirectoryInfo backupdi = Directory.CreateDirectory(backupdir);

                            //Create necessary subdirectorys
                            Directory.CreateDirectory(backupdir + "\\update");
                            Directory.CreateDirectory(backupdir + "\\x64");
                            Directory.CreateDirectory(backupdir + "\\x64\\audio");
                            Directory.CreateDirectory(backupdir + "\\x64\\audio\\sfx");

                            //Backup original Files in Directory (used for restoration later, if user wants to uninstall redux)
                            String[] fileArray = new String[] { "update\\update.rpf", "x64\\audio\\sfx\\RESIDENT.rpf", "x64\\audio\\sfx\\WEAPONS_PLAYER.rpf", "common.rpf", "x64a.rpf", "x64b.rpf" };
                            foreach (var file in fileArray)
                            {
                                if (File.Exists(gtavpath + file))
                                {
                                    File.Move(gtavpath + file, backupdir + "\\" + file);
                                }
                            }

                            //Define assets directory
                            string assetsdir = AppDomain.CurrentDomain.BaseDirectory + "\\assets\\";

                            //Delete if assets have been downloaded before
                            if (Directory.Exists(assetsdir))
                            {
                                Directory.Delete(assetsdir, true);
                            }

                            //Create Assets Directory
                            DirectoryInfo assetsdi = Directory.CreateDirectory(assetsdir);

                            //Get current Redux Version from Webspace
                            WebClient Client = new WebClient();
                            Client.DownloadFile(Options["RDX-DL"], assetsdir + "redux.zip");
                            Client.Dispose();

                            //Unzip Redux into GTA V Path
                            System.IO.Compression.ZipFile.ExtractToDirectory(assetsdir + "redux.zip", gtavpath);

                            //Delete Assets directory
                            assetsdi.Delete(true);

                            //Activate Buttons again
                            CheckForButtons();

                            ReportInfo("Installation erfolgreich abgeschlossen!");
                        }
                        catch (Exception err)
                        {
                            ReportError("Fehler!\n\n" + err);
                        }
                    }
                    else
                    {
                        ReportError("Wir konnten deinen GTA V Installationspfad nicht abrufen!\n\nBitte gebe im Rage-MP Launcher deinen Game Pfad an!");
                        CheckForButtons();
                    }
                }
                else
                {
                    ReportError("Dein GTA V läuft noch!\n\nBitte beende es vor der Installation!");
                    CheckForButtons();
                }
            } else
            {
                ReportError("Du hast schon NVR installiert!\n\nBitte deinstalliere NVR vor der Installation von REDUX!");
                CheckForButtons();
            }
        }
        private void UninstallRedux_Click(object sender, EventArgs e)
        {
            Process[] pname = Process.GetProcessesByName("gta5");
            if (pname.Length == 0)
            {
                //Get GTAV Path from RageMP Registry
                RegistryKey rageReg = Registry.CurrentUser.OpenSubKey(@"SOFTWARE\RAGE-MP", false);
                if (rageReg.GetValueNames().Contains("game_v_path"))
                {
                    //Define GTA V Path - Get from Registry
                    string gtavpath = rageReg.GetValue("game_v_path").ToString() + "\\";

                    //Define backup directory
                    string backupdir = gtavpath + "backupamestris";

                    //Define Files, and iterate trough each of them -> Delete in Base Directory, and move from backups into base
                    String[] fileArray = new String[] { "update\\update.rpf", "x64\\audio\\sfx\\RESIDENT.rpf", "x64\\audio\\sfx\\WEAPONS_PLAYER.rpf", "common.rpf", "x64a.rpf", "x64b.rpf" };
                    foreach (var file in fileArray)
                    {
                        if (File.Exists(gtavpath + file))
                        {
                            File.Delete(gtavpath + file);
                            File.Move(backupdir + "\\" + file, gtavpath + file);
                        }
                    }

                    //Delete Backup
                    Directory.Delete(backupdir, true);

                    //Delete registry value
                    RegistryKey key = Registry.CurrentUser.OpenSubKey("Amestris", true);
                    key.DeleteValue("isReduxInstalled");
                    key.Close();

                    //Activate Buttons again
                    CheckForButtons();
                }
                else
                {
                    ReportError("Wir konnten deinen GTA V Installationspfad nicht abrufen!\n\nBitte gebe im Rage-MP Launcher deinen Game Pfad an!");
                    CheckForButtons();
                }
            }
            else
            {
                ReportError("Dein GTA V läuft noch!\n\nBitte beende es vor der Installation!");
                CheckForButtons();
            }
        }
        private void InstallNVR_Click(object sender, EventArgs e)
        {
            RegistryKey amestrisReg = Registry.CurrentUser.OpenSubKey(@"Amestris", false);
            if (!amestrisReg.GetValueNames().Contains("isReduxInstalled"))
            {
                //Disable Buttons and set Infotext (not via Threadsafe)
                installRedux.Enabled = false;
                uninstallRedux.Enabled = false;
                installNVR.Enabled = false;
                uninstallNVR.Enabled = false;
                installNVR.Text = "Installiert ...";

                //Inform User about Application Hang before starting
                ReportInfo("Während der Installation ist der Launcher nicht verwendbar!\n\nBitte schließe den Launcher nicht!");

                //Sleep for a short period of Time to avoid Thread miscommunication
                Thread.Sleep(2000);

                Process[] pname = Process.GetProcessesByName("gta5");
                if (pname.Length == 0)
                {
                    //Parse Settings, because we need it for the URI later on
                    string settings = AppDomain.CurrentDomain.BaseDirectory + "\\Launcher.xml";
                    if (File.Exists(settings))
                    {
                        try
                        {
                            foreach (XElement el in XElement.Load(File.OpenRead(settings)).Elements())
                            {
                                Options.Add(el.Attribute("key").Value, el.Attribute("value").Value);
                            }
                        }
                        catch
                        {
                            ReportError("Du musst den Launcher zuvor neustarten!");
                            CheckForButtons();
                            return;
                        }
                    }


                    //Set installed Registry Value (moved up here, to be able to activate deinstallation button)
                    RegistryKey key = Registry.CurrentUser.OpenSubKey("Amestris", true);
                    key.SetValue("isNVRInstalled", "true");
                    key.Close();

                    //Get GTAV Path from RageMP Registry
                    RegistryKey rageReg = Registry.CurrentUser.OpenSubKey(@"SOFTWARE\RAGE-MP", false);
                    if (rageReg.GetValueNames().Contains("game_v_path"))
                    {
                        try
                        {
                            //Define GTA Base Directory
                            string gtavpath = rageReg.GetValue("game_v_path").ToString() + "\\";

                            //Define backup directory
                            string backupdir = gtavpath + "backupamestris";

                            //Check if another Mod has been installed before and if a backup got created -> if true, move backup back to root directory
                            if (Directory.Exists(backupdir))
                            {
                                DirectoryInfo backupDirTemp = new DirectoryInfo(backupdir);
                                foreach (var file in backupDirTemp.GetFiles("*", SearchOption.AllDirectories))
                                {
                                    File.Move(backupdir + "\\" + file, gtavpath + file);
                                }

                                Directory.Delete(backupdir, true);
                            }

                            //Create Backup Directory
                            DirectoryInfo backupdi = Directory.CreateDirectory(backupdir);

                            //Create necessary subdirectorys
                            Directory.CreateDirectory(backupdir + "\\update");

                            //Backup original Files in Directory (used for restoration later, if user wants to uninstall nvr)
                            String[] fileArray = new String[] { "update\\update.rpf", "common.rpf" };
                            foreach (var file in fileArray)
                            {
                                if (File.Exists(gtavpath + file))
                                {
                                    File.Move(gtavpath + file, backupdir + "\\" + file);
                                }
                            }

                            //Define assets directory
                            string assetsdir = AppDomain.CurrentDomain.BaseDirectory + "\\assets\\";

                            //Delete if assets have been downloaded before
                            if (Directory.Exists(assetsdir))
                            {
                                Directory.Delete(assetsdir, true);
                            }

                            //Create Assets Directory
                            DirectoryInfo assetsdi = Directory.CreateDirectory(assetsdir);

                            //Get current NVR Version from Webspace
                            WebClient Client = new WebClient();
                            Client.DownloadFile(Options["NVR-DL"], assetsdir + "nvr.zip");
                            Client.Dispose();

                            //Unzip NVR into GTA V Path
                            System.IO.Compression.ZipFile.ExtractToDirectory(assetsdir + "nvr.zip", gtavpath);

                            //Delete Assets directory
                            assetsdi.Delete(true);

                            //Activate Buttons again
                            CheckForButtons();

                            ReportInfo("Installation erfolgreich abgeschlossen!");
                        }
                        catch (Exception err)
                        {
                            ReportError("Fehler!\n\n" + err);
                            CheckForButtons();
                        }
                    }
                    else
                    {
                        ReportError("Wir konnten deinen GTA V Installationspfad nicht abrufen!\n\nBitte gebe im Rage-MP Launcher deinen Game Pfad an!");
                        CheckForButtons();
                    }
                }
                else
                {
                    ReportError("Dein GTA V läuft noch!\n\nBitte beende es vor der Installation!");
                    CheckForButtons();
                }
            } else
            {
                ReportError("Du hast schon Redux installiert!\n\nBitte deinstalliere Redux vor der Installation von NVR!");
                CheckForButtons();
            }
        }
        private void UninstallNVR_Click(object sender, EventArgs e)
        {
            Process[] pname = Process.GetProcessesByName("gta5");
            if (pname.Length == 0)
            {
                //Get GTAV Path from RageMP Registry
                RegistryKey rageReg = Registry.CurrentUser.OpenSubKey(@"SOFTWARE\RAGE-MP", false);
                if (rageReg.GetValueNames().Contains("game_v_path"))
                {
                    //Define GTA V Path - Get from Registry
                    string gtavpath = rageReg.GetValue("game_v_path").ToString() + "\\";

                    //Define backup directory
                    string backupdir = gtavpath + "backupamestris";

                    //Define Files, and iterate trough each of them -> Delete in Base Directory, and move from backups into base
                    String[] fileArray = new String[] { "update\\update.rpf", "common.rpf" };
                    foreach (var file in fileArray)
                    {
                        if (File.Exists(gtavpath + file))
                        {
                            File.Delete(gtavpath + file);
                            File.Move(backupdir + "\\" + file, gtavpath + file);
                        }
                    }

                    //Delete NV Car Cols Folder
                    if (Directory.Exists(gtavpath + "update\\x64\\dlcpacks\\NVR_carcols"))
                    {
                        Directory.Delete(gtavpath + "update\\x64\\dlcpacks\\NVR_carcols", true);
                    }

                    //Delete Backup
                    Directory.Delete(backupdir, true);

                    //Delete registry value
                    RegistryKey key = Registry.CurrentUser.OpenSubKey("Amestris", true);
                    key.DeleteValue("isNVRInstalled");
                    key.Close();

                    //Activate Buttons again
                    CheckForButtons();
                }
                else
                {
                    ReportError("Wir konnten deinen GTA V Installationspfad nicht abrufen!\n\nBitte gebe im Rage-MP Launcher deinen Game Pfad an!");
                    CheckForButtons();
                }
            }
            else
            {
                ReportError("Dein GTA V läuft noch!\n\nBitte beende es vor der Installation!");
                CheckForButtons();
            }
        }

        private static void ReportError(String msg)
        {
            MessageBox.Show(msg, "Fehler!",
                MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private static void ReportInfo(String msg)
        {
            MessageBox.Show(msg, "Info!",
                MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
        public static void ThreadSafe(Action act)
        {
            Dispatcher dispUI = Dispatcher.CurrentDispatcher;
            dispUI.BeginInvoke(act);
        }

        public void CheckForButtons()
        {
            RegistryKey amestrisReg = Registry.CurrentUser.OpenSubKey(@"Amestris", false);
            if (amestrisReg.GetValueNames().Contains("isReduxInstalled"))
            {
                ThreadSafe(() => { installRedux.Enabled = false; });
                ThreadSafe(() => { uninstallRedux.Enabled = true; });
                ThreadSafe(() => { installNVR.Enabled = false; });
                ThreadSafe(() => { uninstallNVR.Enabled = false; });
                ThreadSafe(() => { installRedux.Text = "Bereits installiert!"; });
            }
            else if (amestrisReg.GetValueNames().Contains("isNVRInstalled"))
            {
                ThreadSafe(() => { installRedux.Enabled = false; });
                ThreadSafe(() => { uninstallRedux.Enabled = false; });
                ThreadSafe(() => { installNVR.Enabled = false; });
                ThreadSafe(() => { uninstallNVR.Enabled = true; });
                ThreadSafe(() => { installNVR.Text = "Bereits installiert!"; });
            }
            else
            {
                ThreadSafe(() => { installRedux.Enabled = true; });
                ThreadSafe(() => { uninstallRedux.Enabled = false; });
                ThreadSafe(() => { installNVR.Enabled = true; });
                ThreadSafe(() => { uninstallNVR.Enabled = false; });
                ThreadSafe(() => { installRedux.Text = "Installieren"; });
                ThreadSafe(() => { installNVR.Text = "Installieren"; });
            }
        }

        private void NextPage_Click(object sender, EventArgs e)
        {
            ThreadSafe(() => { Control[] Co = TopLevelControl.Controls.Find("extrasThird1", true); Co[0].Show(); });
            ThreadSafe(() => { Control[] Co = TopLevelControl.Controls.Find("extrasThird1", true); Co[0].BringToFront(); });
        }
    }
}
