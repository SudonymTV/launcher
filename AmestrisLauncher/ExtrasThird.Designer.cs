﻿namespace AmestrisLauncher
{
    partial class ExtrasThird
    {
        /// <summary> 
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Komponenten-Designer generierter Code

        /// <summary> 
        /// Erforderliche Methode für die Designerunterstützung. 
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ExtrasThird));
            this.uninstallRoads = new System.Windows.Forms.Button();
            this.installRoads = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // uninstallRoads
            // 
            this.uninstallRoads.BackColor = System.Drawing.Color.Transparent;
            this.uninstallRoads.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.uninstallRoads.Font = new System.Drawing.Font("Bahnschrift SemiBold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uninstallRoads.ForeColor = System.Drawing.Color.White;
            this.uninstallRoads.Location = new System.Drawing.Point(354, 247);
            this.uninstallRoads.Name = "uninstallRoads";
            this.uninstallRoads.Size = new System.Drawing.Size(138, 51);
            this.uninstallRoads.TabIndex = 29;
            this.uninstallRoads.TabStop = false;
            this.uninstallRoads.Text = "Deinstallieren";
            this.uninstallRoads.UseVisualStyleBackColor = false;
            this.uninstallRoads.Click += new System.EventHandler(this.UninstallRoads_Click);
            // 
            // installRoads
            // 
            this.installRoads.BackColor = System.Drawing.Color.Transparent;
            this.installRoads.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.installRoads.Font = new System.Drawing.Font("Bahnschrift SemiBold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.installRoads.ForeColor = System.Drawing.Color.White;
            this.installRoads.Location = new System.Drawing.Point(210, 247);
            this.installRoads.Name = "installRoads";
            this.installRoads.Size = new System.Drawing.Size(138, 51);
            this.installRoads.TabIndex = 27;
            this.installRoads.TabStop = false;
            this.installRoads.Text = "Installieren";
            this.installRoads.UseVisualStyleBackColor = false;
            this.installRoads.Click += new System.EventHandler(this.InstallRoads_Click);
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Bahnschrift", 18F, System.Drawing.FontStyle.Bold);
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(-78, 105);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(859, 31);
            this.label2.TabIndex = 23;
            this.label2.Text = "Modifikation - L.A. Roads";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Bahnschrift SemiBold", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(-75, 118);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(859, 27);
            this.label3.TabIndex = 25;
            this.label3.Text = "___________________________________________";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Bahnschrift SemiBold", 14.25F, System.Drawing.FontStyle.Bold);
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(-78, 139);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(859, 49);
            this.label1.TabIndex = 24;
            this.label1.Text = "L.A. Roads ist eine Mod, die das Aussehen der Straßen verändert.\r\nDiese sind nach" +
    " realistischen Gegebenheiten gerichtet.";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label7
            // 
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Bahnschrift", 28F, System.Drawing.FontStyle.Bold);
            this.label7.ForeColor = System.Drawing.Color.White;
            this.label7.Location = new System.Drawing.Point(-90, 34);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(864, 46);
            this.label7.TabIndex = 26;
            this.label7.Text = "EXTRAS";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label9
            // 
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Bahnschrift", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.White;
            this.label9.Location = new System.Drawing.Point(-74, 179);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(873, 72);
            this.label9.TabIndex = 28;
            this.label9.Text = resources.GetString("label9.Text");
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ExtrasThird
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(75)))), ((int)(((byte)(75)))), ((int)(((byte)(75)))), ((int)(((byte)(75)))));
            this.Controls.Add(this.uninstallRoads);
            this.Controls.Add(this.installRoads);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label9);
            this.Name = "ExtrasThird";
            this.Size = new System.Drawing.Size(915, 627);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button uninstallRoads;
        private System.Windows.Forms.Button installRoads;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label9;
    }
}
