﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Net;
using System.Runtime.InteropServices;
using System.Timers;
using System.Windows.Forms;
using System.Xml.Linq;

namespace Updater
{
    public partial class Updater : Form
    {
        //Rounded Corners
        [DllImport("Gdi32.dll", EntryPoint = "CreateRoundRectRgn")]
        private static extern IntPtr CreateRoundRectRgn
        (
            int nLeftRect,     // x-coordinate of upper-left corner
            int nTopRect,      // y-coordinate of upper-left corner
            int nRightRect,    // x-coordinate of lower-right corner
            int nBottomRect,   // y-coordinate of lower-right corner
            int nWidthEllipse, // height of ellipse
            int nHeightEllipse // width of ellipse
        );
        public Updater()
        {
            InitializeComponent();
            Region = System.Drawing.Region.FromHrgn(CreateRoundRectRgn(0, 0, Width, Height, 5, 5));
            this.Name = "AmestrisRP - Updater";
            this.Icon = Properties.Resources.Icon;
            System.Timers.Timer updateTimer = new System.Timers.Timer();
            updateTimer.Elapsed += new ElapsedEventHandler(DoUpdate);
            updateTimer.Interval = 5000;
            updateTimer.Enabled = true;
        }

        private static void DoUpdate(object source, ElapsedEventArgs e)
        {
            Dictionary<String, String> Options = new Dictionary<String, String>();
            Dictionary<String, String> LocalVersion = new Dictionary<String, String>();
            if (File.Exists(AppDomain.CurrentDomain.BaseDirectory + "\\Launcher.xml"))
            {
                File.Delete(AppDomain.CurrentDomain.BaseDirectory + "\\Launcher.xml");
            }
            WebClient launcherSettingsDL = new WebClient();
            launcherSettingsDL.DownloadFile("http://patch.amestrisrp.com/SETTINGS/Launcher.xml", "Launcher.xml");
            var settings = AppDomain.CurrentDomain.BaseDirectory + "\\Launcher.xml";
            if (File.Exists(settings))
            {
                try
                {
                    foreach (var el in XElement.Load(File.OpenRead(settings)).Elements())
                    {
                        Options.Add(el.Attribute("key").Value, el.Attribute("value").Value);
                    }
                }
                catch { ReportError("Ein Fehler ist während dem übernehmen der Einstellungen aufgetreten!"); return; }
            }
            else
            {
                ReportError("Kann 'Launcher.xml' nicht finden!");
                return;
            }
            int remVersion = Int32.Parse(Options["remVer"]);
            var localVersionPath = AppDomain.CurrentDomain.BaseDirectory + "\\LocalVersion.xml";
            if (!File.Exists(localVersionPath))
            {
                XElement root = new XElement("options");
                XDocument doc = new XDocument(root);
                XElement element = new XElement("option");
                element.SetAttributeValue("key", "version");
                element.SetAttributeValue("value", "0");
                doc.Root.Add(element);
                doc.Save(localVersionPath);
            }
            else
            {
                try
                {
                    foreach (var el in XElement.Load(File.OpenRead(localVersionPath)).Elements())
                    {
                        LocalVersion.Add(el.Attribute("key").Value, el.Attribute("value").Value);
                    }
                }
                catch { ReportError("Ein Fehler ist während dem setzen der Lokalen Version aufgetreten!"); return; }
            }
            try
            {
                foreach (var el in XElement.Load(File.OpenRead(localVersionPath)).Elements())
                {
                    LocalVersion.Add(el.Attribute("key").Value, el.Attribute("value").Value);
                }
            }
            catch (Exception error)
            {
                Console.WriteLine(error);
            }
            int localVersion = Int32.Parse(LocalVersion["version"]);
            if (localVersion < remVersion)
            {
                string dl = Options["L-DL"];
                WebClient launcherDL = new WebClient();
                launcherDL.DownloadFile(dl, "update.zip");

                string zipToUnpack = "update.zip";
                string unpackDirectory = AppDomain.CurrentDomain.BaseDirectory + @"\";
                using (ZipArchive archive = ZipFile.OpenRead(zipToUnpack))
                {
                    foreach (ZipArchiveEntry entry in archive.Entries)
                    {
                        string completeFileName = Path.Combine(unpackDirectory, entry.FullName);
                        string directory = Path.GetDirectoryName(completeFileName);

                        Console.WriteLine(completeFileName);
                        Console.WriteLine(directory);


                        if (!Directory.Exists(directory))
                        {
                            Directory.CreateDirectory(directory);
                        }
                        entry.ExtractToFile(completeFileName, true);
                    }
                }
            }
            else
            {
                if (File.Exists("update.zip"))
                {
                    File.Delete("update.zip");
                }
            }
            if (File.Exists("update.zip"))
            {
                File.Delete("update.zip");
            }
            string tempUpdate = AppDomain.CurrentDomain.BaseDirectory + "\\.updaterOpened";
            using (File.Create(tempUpdate)) { }
            ProcessStartInfo launcherProcFailsafe = new ProcessStartInfo("AmestrisLauncher.exe");
            Process.Start(launcherProcFailsafe);
            Process.GetCurrentProcess().Kill();
        }
        private static void ReportError(String msg)
        {
            MessageBox.Show(msg, "Fehler!",
                MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
    }
}
