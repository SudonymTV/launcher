﻿using System;
using System.Windows.Forms;
using Microsoft.Win32;

namespace Updater
{
    public class Program
    {
        [STAThread]
        public static void Main()
        {
            if (Registry.CurrentUser.OpenSubKey("Amestris", true) != null)
            {
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(new Updater());
            } else
            {
                Microsoft.Win32.RegistryKey rkey;
                rkey = Microsoft.Win32.Registry.CurrentUser.CreateSubKey("Amestris");
                rkey.SetValue("isInit", "true");
                rkey.Close();
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(new Updater());
            }
        }
    }
}
